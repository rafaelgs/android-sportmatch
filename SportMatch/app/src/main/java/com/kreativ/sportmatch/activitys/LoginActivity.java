package com.kreativ.sportmatch.activitys;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.annotation.TargetApi;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.backendless.Backendless;
import com.backendless.BackendlessUser;
import com.backendless.async.callback.BackendlessCallback;
import com.backendless.exceptions.BackendlessFault;
import com.backendless.persistence.local.UserTokenStorageFactory;
import com.facebook.CallbackManager;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.kreativ.sportmatch.R;
import com.kreativ.sportmatch.fragments.LoginMainFragment;


public class LoginActivity extends AppCompatActivity {

    private String TAG = "LOGIN_ACTIVITY";

    // UI references.
    private EditText mPasswordView, mNameView, mEmailView, mUsernameView;
    private View mProgressView;
    private View mLoginFormView;
    private boolean signUpAction = false;

    private CallbackManager callbackManager;

    private boolean btnEnterPressed = false, btnCreatePressed = false;
    private Button btnSignUp, btnSignIn, btnFacebookLogin;
    private SharedPreferences sharedPreferences;

    private FirebaseAuth mAuth;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        // Busca instancia do Firebase
        mAuth = FirebaseAuth.getInstance();

        // Faz a conexão com o Backendless
        Backendless.initApp(this, getResources().getString(R.string.back_app_id), getResources().getString(R.string.back_android_secret_key), getResources().getString(R.string.back_app_version));

        // Verifica se o usuário já está logado
        String userToken = UserTokenStorageFactory.instance().getStorage().get();
        if (userToken != null && !userToken.equals( "" )) {
            // Pula a tela de login
            Intent intent = new Intent(this, MainActivity.class);
            startActivity(intent);
            finish();
        }

        // Inicializa as preferencias
        sharedPreferences = getPreferences(Context.MODE_PRIVATE);

        ActionBar actionBar = this.getSupportActionBar();
        actionBar.hide();

        getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.container, new LoginMainFragment())
                .commit();

        // Define o BG
        /*ImageView imageView = (ImageView) findViewById(R.id.imgBackground);
        imageView.setImageResource(0);
        Drawable draw = getResources().getDrawable(R.drawable.bg_login_blur_big);
        draw = resize(draw);
        imageView.setImageDrawable(draw);

        // Set up the login form.
        mUsernameView = (EditText) findViewById(R.id.username);
        mPasswordView = (EditText) findViewById(R.id.password);

        mLoginFormView = findViewById(R.id.login_form);
        mProgressView = findViewById(R.id.login_progress);

        mNameView = (EditText) findViewById(R.id.name);
        mEmailView = (EditText) findViewById(R.id.email);

        btnSignIn = (Button) findViewById(R.id.email_sign_in_button);
        btnSignIn.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                if (btnEnterPressed) {
                    attemptLogin();
                } else {
                    btnEnterPressed = true;

                    mUsernameView.setVisibility(View.VISIBLE);
                    mPasswordView.setVisibility(View.VISIBLE);

                    btnSignUp.setVisibility(View.GONE);
                    btnFacebookLogin.setVisibility(View.GONE);
                }
            }
        });

        btnSignUp = (Button) findViewById(R.id.btn_sign_up);
        btnSignUp.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if (btnCreatePressed) {
                    signUpUser();
                } else {
                    btnCreatePressed = true;

                    mUsernameView.setVisibility(View.VISIBLE);
                    mPasswordView.setVisibility(View.VISIBLE);
                    mNameView.setVisibility(View.VISIBLE);
                    mEmailView.setVisibility(View.VISIBLE);

                    btnSignIn.setVisibility(View.GONE);
                    btnFacebookLogin.setVisibility(View.GONE);
                }
            }
        });

        // Login com facebook
        FacebookSdk.sdkInitialize(this.getApplicationContext());
        callbackManager = CallbackManager.Factory.create();

        btnFacebookLogin = (Button) findViewById(R.id.btnFacebookLogin);
        btnFacebookLogin.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(LoginActivity.this, "Função em desenvolvimento", Toast.LENGTH_SHORT).show();

                // Comentado para o Imagine Cup
//                Map<String, String> facebookFieldsMappings = new HashMap<>();
//                facebookFieldsMappings.put("email", "email");
//                facebookFieldsMappings.put("name", "name");
//                facebookFieldsMappings.put("gender", "fb_gender");
//
//                List<String> permissions = new ArrayList<>();
//                permissions.add("email");
//                permissions.add("public_profile");
//
//                showProgress(true);
//
//                Backendless.UserService.loginWithFacebookSdk(LoginActivity.this, facebookFieldsMappings, permissions, callbackManager, new AsyncCallback<BackendlessUser>() {
//                    @Override
//                    public void handleResponse(BackendlessUser user) {
//                        //Toast.makeText(LoginActivity.this, "Ok, User ID =" + backendlessUser.getUserId(), Toast.LENGTH_LONG).show();
//
//                        showProgress(false);
//
//                        SharedPreferences.Editor editor = sharedPreferences.edit();
//                        editor.putString(getString(R.string.pref_user_name), user.getProperty(getString(R.string.back_user_name)).toString());
//                        editor.putString(getString(R.string.pref_user_email), user.getEmail().toString());
//                        editor.commit();
//
//                        Intent intent = new Intent(LoginActivity.this, MainActivity.class);
//                        startActivity(intent);
//                    }
//
//                    @Override
//                    public void handleFault(BackendlessFault backendlessFault) {
//                        showProgress(false);
//                        Snackbar.make(findViewById(android.R.id.content), getString(R.string.error_login_failed), Snackbar.LENGTH_SHORT).show();
//                    }
//                });
            }
        });

        LinearLayout layoutFocus = (LinearLayout) findViewById(R.id.layoutFocus);
        layoutFocus.requestFocus();*/
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        callbackManager.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public void onBackPressed() {
//        // Reinicia os parametros da tela
//        mUsernameView.setVisibility(View.GONE);
//        mPasswordView.setVisibility(View.GONE);
//        mNameView.setVisibility(View.GONE);
//        mEmailView.setVisibility(View.GONE);
//
//        btnSignIn.setVisibility(View.VISIBLE);
//        btnSignUp.setVisibility(View.VISIBLE);
//        btnFacebookLogin.setVisibility(View.VISIBLE);
//
//        btnEnterPressed = false;
//        btnCreatePressed = false;
    }

    private Drawable resize(Drawable image) {
        Bitmap bitmap = ((BitmapDrawable) image).getBitmap();
        Bitmap bitmapResized = Bitmap.createScaledBitmap(bitmap,
                (int) (bitmap.getWidth() * 0.5), (int) (bitmap.getHeight() * 0.5), false);
        return new BitmapDrawable(getResources(), bitmapResized);
    }

    public void signUpUser() {
        if(signUpAction) {
            signUpAction = false;

            BackendlessUser user = new BackendlessUser();
            user.setProperty("email", mEmailView.getText().toString());
            user.setProperty("username", mUsernameView.getText().toString());
            user.setProperty("name", mNameView.getText().toString());
            user.setPassword(mPasswordView.getText().toString());

            showProgress(true);
            Backendless.UserService.register(user, new BackendlessCallback<BackendlessUser>() {
                @Override
                public void handleResponse(BackendlessUser user) {
                    showProgress(false);

                    SharedPreferences.Editor editor = sharedPreferences.edit();
                    editor.putString(getString(R.string.pref_user_name), user.getProperty(getString(R.string.back_user_name)).toString());
                    editor.putString(getString(R.string.pref_user_email), user.getEmail().toString());
                    editor.commit();

                    Intent intent = new Intent(LoginActivity.this, MainActivity.class);
                    startActivity(intent);
                }

                @Override
                public void handleFault(BackendlessFault fault) {
                    showProgress(false);
                    Snackbar.make(findViewById(android.R.id.content), getString(R.string.error_login_failed), Snackbar.LENGTH_SHORT).show();
                }
            });

        } else {
            signUpAction = true;

            mNameView.setVisibility(View.VISIBLE);
            mEmailView.setVisibility(View.VISIBLE);
        }
    }

    public void signUpUser2() {
        mAuth.createUserWithEmailAndPassword(mEmailView.getText().toString(), mPasswordView.getText().toString())
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        Log.d(TAG, "createUserWithEmail:onComplete:" + task.isSuccessful());

                        if (!task.isSuccessful()) {
                            Toast.makeText(LoginActivity.this, "Authentication failed.", Toast.LENGTH_SHORT).show();
                        }
                    }
                });
    }

    private void attemptLogin() {
        // Reset errors.
        mUsernameView.setError(null);
        mPasswordView.setError(null);

        // Store values at the time of the login attempt.
        String username = mUsernameView.getText().toString();
        String password = mPasswordView.getText().toString();

        boolean cancel = false;
        View focusView = null;

        // Check for a valid password, if the user entered one.
        if (!TextUtils.isEmpty(password) && !isPasswordValid(password)) {
            mPasswordView.setError(getString(R.string.error_invalid_password));
            focusView = mPasswordView;
            cancel = true;
        }

        // Check for a valid username address.
        if (TextUtils.isEmpty(username)) {
            mUsernameView.setError(getString(R.string.error_field_required));
            focusView = mUsernameView;
            cancel = true;
        }

        if (cancel) {
            // There was an error; don't attempt login and focus the first
            // form field with an error.
            focusView.requestFocus();
        } else {
            // Show a progress spinner, and kick off a background task to
            // perform the user login attempt.
            showProgress(true);

//            mAuth.signInWithEmailAndPassword(mEmailView.getText().toString(), mPasswordView.getText().toString())
//                    .addOnCompleteListener(new OnCompleteListener<AuthResult>() {
//                        @Override
//                        public void onComplete(@NonNull Task<AuthResult> task) {
//                            if (task.isSuccessful()) {
//                                Snackbar.make(findViewById(android.R.id.content), "Logado com sucesso!", Snackbar.LENGTH_SHORT).show();
//                            }
//                        }
//                    });

            Backendless.UserService.login(username, password, new BackendlessCallback<BackendlessUser>() {
                @Override
                public void handleResponse(BackendlessUser user) {
                    showProgress(false);

                    SharedPreferences.Editor editor = sharedPreferences.edit();
                    editor.putString(getString(R.string.pref_user_name), user.getProperty(getString(R.string.back_user_name)).toString());
                    editor.putString(getString(R.string.pref_user_email), user.getEmail().toString());
                    editor.commit();

                    Intent intent = new Intent(LoginActivity.this, MainActivity.class);
                    startActivity(intent);
                }

                @Override
                public void handleFault(BackendlessFault fault) {
                    showProgress(false);
                    Snackbar.make(findViewById(android.R.id.content), getString(R.string.error_login_failed), Snackbar.LENGTH_SHORT).show();
                }
            }, true);
        }
    }

    private boolean isPasswordValid(String password) {
        //TODO: Replace this with your own logic
        return password.length() > 4;
    }

    /**
     * Shows the progress UI and hides the login form.
     */
    @TargetApi(Build.VERSION_CODES.HONEYCOMB_MR2)
    private void showProgress(final boolean show) {
        // On Honeycomb MR2 we have the ViewPropertyAnimator APIs, which allow
        // for very easy animations. If available, use these APIs to fade-in
        // the progress spinner.
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR2) {
            int shortAnimTime = getResources().getInteger(android.R.integer.config_shortAnimTime);

            mLoginFormView.setVisibility(show ? View.GONE : View.VISIBLE);
            mLoginFormView.animate().setDuration(shortAnimTime).alpha(
                    show ? 0 : 1).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    mLoginFormView.setVisibility(show ? View.GONE : View.VISIBLE);
                }
            });

            mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
            mProgressView.animate().setDuration(shortAnimTime).alpha(
                    show ? 1 : 0).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
                }
            });
        } else {
            // The ViewPropertyAnimator APIs are not available, so simply show
            // and hide the relevant UI components.
            mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
            mLoginFormView.setVisibility(show ? View.GONE : View.VISIBLE);
        }
    }
}

