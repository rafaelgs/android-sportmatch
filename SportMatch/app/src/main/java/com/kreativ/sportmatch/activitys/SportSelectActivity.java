package com.kreativ.sportmatch.activitys;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;

import com.kreativ.sportmatch.R;

public class SportSelectActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sport_select_activity);

        Button btnFutsal = (Button) findViewById(R.id.btnFutsal);
        btnFutsal.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                redirect(Integer.parseInt(getString(R.string.sport_code_futsal)));
            }
        });

        Button btnSociety = (Button) findViewById(R.id.btnSociety);
        btnSociety.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                redirect(Integer.parseInt(getString(R.string.sport_code_society)));
            }
        });

        Button btnSoccer = (Button) findViewById(R.id.btnSoccer);
        btnSoccer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                redirect(Integer.parseInt(getString(R.string.sport_code_soccer)));
            }
        });

        Button btnVolley = (Button) findViewById(R.id.btnVolley);
        btnVolley.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                redirect(Integer.parseInt(getString(R.string.sport_code_volley)));
            }
        });

        Button btnBasquete = (Button) findViewById(R.id.btnBasquete);
        btnBasquete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                redirect(Integer.parseInt(getString(R.string.sport_code_basket)));
            }
        });

        Button btnHandball = (Button) findViewById(R.id.btnHandball);
        btnHandball.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                redirect(Integer.parseInt(getString(R.string.sport_code_handball)));
            }
        });

        Button btnPeteca = (Button) findViewById(R.id.btnPeteca);
        btnPeteca.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                redirect(Integer.parseInt(getString(R.string.sport_code_peteca)));
            }
        });

        Button btnTenis = (Button) findViewById(R.id.btnTenis);
        btnTenis.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                redirect(Integer.parseInt(getString(R.string.sport_code_tennis)));
            }
        });

        Button btnPaintball = (Button) findViewById(R.id.btnPaintball);
        btnPaintball.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                redirect(Integer.parseInt(getString(R.string.sport_code_paintball)));
            }
        });

        Button btnBaseball = (Button) findViewById(R.id.btnBaseball);
        btnBaseball.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                redirect(Integer.parseInt(getString(R.string.sport_code_baseball)));
            }
        });

        Button btnGolf = (Button) findViewById(R.id.btnGolf);
        btnGolf.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                redirect(Integer.parseInt(getString(R.string.sport_code_golf)));
            }
        });

        Button btnFootball = (Button) findViewById(R.id.btnFootball);
        btnFootball.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               redirect(Integer.parseInt(getString(R.string.sport_code_american)));
            }
        });

        Button btnRugby = (Button) findViewById(R.id.btnRugby);
        btnRugby.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                redirect(Integer.parseInt(getString(R.string.sport_code_rugby)));
            }
        });

        Button btnOthers = (Button) findViewById(R.id.btnOthers);
        btnOthers.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                redirect(Integer.parseInt(getString(R.string.sport_code_other)));
            }
        });
    }

    public void redirect (int sportCode){
        Intent intent = new Intent(SportSelectActivity.this, EditMatchActivity.class);
        intent.putExtra("exSport", sportCode);
        startActivity(intent);
        finish();
    }

    @Override
    public void onBackPressed() {
        finish();
    }
}


