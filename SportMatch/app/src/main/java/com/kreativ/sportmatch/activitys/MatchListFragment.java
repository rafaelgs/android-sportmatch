package com.kreativ.sportmatch.activitys;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.backendless.Backendless;
import com.backendless.BackendlessCollection;
import com.backendless.persistence.BackendlessDataQuery;
import com.backendless.persistence.local.UserIdStorageFactory;
import com.kreativ.sportmatch.R;
import com.kreativ.sportmatch.adapters.RecyclerItemClickListener;
import com.kreativ.sportmatch.adapters.RecyclerListAdapter;
import com.kreativ.sportmatch.dummy.DummyContent.DummyItem;
import com.kreativ.sportmatch.models.Match;
import com.kreativ.sportmatch.models.MatchPlayers;

import java.util.ArrayList;
import java.util.List;

/**
 * A fragment representing a list of Items.
 * <p/>
 * Activities containing this fragment MUST implement the {@link OnListFragmentInteractionListener}
 * interface.
 */
public class MatchListFragment extends Fragment {

    // TODO: Customize parameter argument names
    private static final String ARG_COLUMN_COUNT = "column-count";
    // TODO: Customize parameters
    private int mColumnCount = 1;
    private OnListFragmentInteractionListener mListener;
    private BackendlessCollection<MatchPlayers> backColl;

    private RecyclerView recyclerView;
    private int queryType = 0;
    private List<Match> matchList;
    private LinearLayout screenLoading;

    /**
     * Mandatory empty constructor for the fragment manager to instantiate the
     * fragment (e.g. upon screen orientation changes).
     */
    public MatchListFragment() {
    }

    @SuppressLint("ValidFragment")
    public MatchListFragment(int queryType) {
        this.queryType = queryType;
    }

    // TODO: Customize parameter initialization
    @SuppressWarnings("unused")
    public static MatchListFragment newInstance(int columnCount, BackendlessCollection<MatchPlayers> backColl) {
        MatchListFragment fragment = new MatchListFragment(0);
        Bundle args = new Bundle();
        args.putInt(ARG_COLUMN_COUNT, columnCount);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (getArguments() != null) {
            mColumnCount = getArguments().getInt(ARG_COLUMN_COUNT);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_match_list, container, false);
        screenLoading = (LinearLayout) view.findViewById(R.id.screenLoading);

        // Set the adapter
        Context context = view.getContext();
        recyclerView = (RecyclerView) view.findViewById(R.id.list);
        if (mColumnCount <= 1) {
            recyclerView.setLayoutManager(new LinearLayoutManager(context));
        } else {
            recyclerView.setLayoutManager(new GridLayoutManager(context, mColumnCount));
        }

        AsyncTask task = new AsyncTask() {
            @Override
            protected Object doInBackground(Object[] params) {
                String userId = UserIdStorageFactory.instance().getStorage().get();

                if (queryType == 1) {
                    // Gerenciar
                    String whereClause = "owner.objectId = '"+userId+"'";

                    BackendlessDataQuery dataQuery = new BackendlessDataQuery();
                    dataQuery.setWhereClause(whereClause);

                    BackendlessCollection<Match> result = Backendless.Persistence.of(Match.class).find(dataQuery);
                    return result;
                } else {
                    // Convidado
                    String whereClause = "user.objectId = '"+userId+"'";

                    BackendlessDataQuery dataQuery = new BackendlessDataQuery();
                    dataQuery.setWhereClause(whereClause);

                    BackendlessCollection<MatchPlayers> result = Backendless.Persistence.of(MatchPlayers.class).find(dataQuery);
                    return result;
                }

            }

            @Override
            protected void onPostExecute(Object o) {

                matchList = new ArrayList<>();

                if (queryType == 1) {
                    BackendlessCollection<Match> backColl = (BackendlessCollection<Match>) o;
                    matchList = backColl.getData();
                } else if (queryType == 2) {
                    BackendlessCollection<MatchPlayers> backColl = (BackendlessCollection<MatchPlayers>) o;

                    for (int i = 0; i < backColl.getData().size(); i++) {
                        matchList.add(backColl.getData().get(i).getMatch());
                    }
                }

                recyclerView.setAdapter(new RecyclerListAdapter(getActivity().getApplicationContext(), matchList));
                recyclerView.addOnItemTouchListener(
                        new RecyclerItemClickListener(getContext(), new RecyclerItemClickListener.OnItemClickListener() {
                            @Override public void onItemClick(View view, int position) {
                                Match match = matchList.get(position);

                                Intent intent = new Intent(getContext(), MatchDetailActivity.class);
                                intent.putExtra("exMatchId", match.getObjectId());
                                intent.putExtra("exMatchName", match.getName());
                                intent.putExtra("exSport", match.getSport());
                                startActivity(intent);
                            }
                        })
                );

                // Esconde tela de loading
                screenLoading.setVisibility(View.GONE);
                recyclerView.setVisibility(View.VISIBLE);
            }
        };
        task.execute();

        return view;
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnListFragmentInteractionListener) {
            mListener = (OnListFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnListFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p/>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnListFragmentInteractionListener {
        // TODO: Update argument type and name
        void onListFragmentInteraction(DummyItem item);
    }
}
