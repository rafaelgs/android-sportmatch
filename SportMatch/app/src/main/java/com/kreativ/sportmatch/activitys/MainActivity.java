package com.kreativ.sportmatch.activitys;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.backendless.Backendless;
import com.backendless.BackendlessUser;
import com.backendless.async.callback.AsyncCallback;
import com.backendless.async.callback.BackendlessCallback;
import com.backendless.exceptions.BackendlessFault;
import com.backendless.persistence.local.UserIdStorageFactory;
import com.kreativ.sportmatch.R;
import com.kreativ.sportmatch.dummy.DummyContent;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener, MatchListFragment.OnListFragmentInteractionListener {

    private FragmentManager fm;
    private TextView txtUsername, txtUserEmail;
    private boolean exit = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle("");
       // setSupportActionBar(toolbar);

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        fm = getSupportFragmentManager();

        MainFragment mainFragmet = new MainFragment();
        fm.beginTransaction().replace(R.id.flContent, mainFragmet).commit();

        String userId = UserIdStorageFactory.instance().getStorage().get();

        // Define os dados do usuário no drawer
        View headerView = navigationView.getHeaderView(0);
        txtUsername = (TextView) headerView.findViewById(R.id.txt_user_name);
        txtUserEmail = (TextView) headerView.findViewById(R.id.txt_user_email);

        Backendless.UserService.findById(userId, new AsyncCallback<BackendlessUser>() {
            @Override
            public void handleResponse(BackendlessUser response) {
                if (response != null) {
                    Backendless.UserService.setCurrentUser(response);
                    txtUsername.setText(response.getProperty(getString(R.string.back_user_name)).toString());
                    txtUserEmail.setText(response.getEmail());
                    Log.e("USER_TEST", "--------------- Usuário encontrado");
                }
            }

            @Override

            public void handleFault(BackendlessFault fault) {
                Log.e("USER_TEST", "--------------- Usuário não encontrado!");
            }

        });
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            if (exit) {
                finish();
            } else {
                Toast.makeText(MainActivity.this, getString(R.string.action_press_exit), Toast.LENGTH_SHORT).show();
                exit = true;
            }
        }
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_manage_match) {
            MatchListFragment matchListFragment = new MatchListFragment(1);
            fm.beginTransaction().replace(R.id.flContent, matchListFragment).commit();
        }  else if (id == R.id.nav_next_match) {
            MatchListFragment matchListFragment = new MatchListFragment(2);
            fm.beginTransaction().replace(R.id.flContent, matchListFragment).commit();
        } else if (id == R.id.nav_create_math) {
            Intent intent = new Intent(this, SportSelectActivity.class);
            startActivity(intent);
        } else if (id == R.id.nav_find_match) {
            Intent intent = new Intent(this, SearchMatchActivity.class);
            startActivity(intent);
        } else if (id == R.id.nav_home) {
            MainFragment mainFragment = new MainFragment();
            fm.beginTransaction().replace(R.id.flContent, mainFragment).commit();
        } else if (id == R.id.nav_logout) {
            Backendless.UserService.logout(new BackendlessCallback<Void>() {
                @Override
                public void handleResponse(Void response) {
                    Intent intent = new Intent(MainActivity.this, LoginActivity.class);
                    startActivity(intent);
                    finish();
                }

                @Override
                public void handleFault(BackendlessFault fault) {
                    Toast.makeText(MainActivity.this, getString(R.string.error_logout), Toast.LENGTH_SHORT).show();
                }
            });
        }

//        if (id == R.id.nav_camera) {
//            // Handle the camera action
//        } else if (id == R.id.nav_gallery) {
//
//        } else if (id == R.id.nav_slideshow) {
//
//        } else if (id == R.id.nav_manage) {
//
//        } else if (id == R.id.nav_share) {
//
//        } else if (id == R.id.nav_send) {
//
//        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    @Override
    public void onListFragmentInteraction(DummyContent.DummyItem item) {
        Toast.makeText(this,"onListFragmentInteraction",Toast.LENGTH_SHORT).show();
    }
}
