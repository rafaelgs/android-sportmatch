package com.kreativ.sportmatch.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.kreativ.sportmatch.R;
import com.kreativ.sportmatch.models.MatchPlayers;

import java.text.SimpleDateFormat;
import java.util.List;
import java.util.Locale;

/**
 * Created by Rafael on 20/02/2016.
 */
public class GuestListAdapter extends BaseAdapter {

    private List<MatchPlayers> matchList;
    private Context context;

    public GuestListAdapter(Context mContext, List<MatchPlayers> matchList) {
        this.matchList = matchList;
        context = mContext;
    }

    @Override
    public int getCount() {
        return matchList.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View rootView = inflater.inflate(android.R.layout.simple_list_item_1, parent, false);

        // Define o nome do participante
        TextView txtGuestName = (TextView) rootView.findViewById(android.R.id.text1);
        txtGuestName.setText(matchList.get(position).getUser().getProperty(context.getString(R.string.back_user_name)).toString());

        return rootView;
    }
}
