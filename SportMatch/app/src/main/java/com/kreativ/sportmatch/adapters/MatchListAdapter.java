package com.kreativ.sportmatch.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.kreativ.sportmatch.R;
import com.kreativ.sportmatch.models.Match;
import com.kreativ.sportmatch.models.MatchPlayers;

import java.text.SimpleDateFormat;
import java.util.List;
import java.util.Locale;

/**
 * Created by Rafael on 20/02/2016.
 */
public class MatchListAdapter extends BaseAdapter {

    private List<Match> matchList;
    private Context context;

    public MatchListAdapter(Context mContext, List<Match> matchList) {
        this.matchList = matchList;
        context = mContext;
    }

    @Override
    public int getCount() {
        return matchList.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View rootView = inflater.inflate(R.layout.list_item_match, parent, false);

        // Define o titulo
        TextView txtMatchName = (TextView) rootView.findViewById(R.id.txtMatchTitle);
        txtMatchName.setText(matchList.get(position).getName());

        // Define a data
        TextView txtMatchDate = (TextView) rootView.findViewById(R.id.txtMatchDate);
        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM", Locale.US);
        String date = sdf.format(matchList.get(position).getDateStart());
        txtMatchDate.setText(date);

        // Define o icone
        int sportCode = matchList.get(position).getSport();
        int sportIcon = 0;
        int sportIconBg = 0;
        if(sportCode == Integer.parseInt(context.getString(R.string.sport_code_futsal))){
            sportIcon = R.drawable.ic_futsal;
            sportIconBg = R.drawable.round_corner_layout_futsal;
        } else if(sportCode == Integer.parseInt(context.getString(R.string.sport_code_society))){
            sportIcon = R.drawable.ic_fut_society;
            sportIconBg = R.drawable.round_corner_layout_society;
        } else if(sportCode == Integer.parseInt(context.getString(R.string.sport_code_soccer))){
            sportIcon = R.drawable.ic_soccer;
            sportIconBg = R.drawable.round_corner_layout_soccer;
        } else if(sportCode == Integer.parseInt(context.getString(R.string.sport_code_volley))) {
            sportIcon = R.drawable.ic_volei;
            sportIconBg = R.drawable.round_corner_layout_volley;
        } else if(sportCode == Integer.parseInt(context.getString(R.string.sport_code_basket))){
            sportIcon = R.drawable.ic_basquete;
            sportIconBg = R.drawable.round_corner_layout_basquete;
        } else if(sportCode == Integer.parseInt(context.getString(R.string.sport_code_handball))){
            sportIcon = R.drawable.ic_handball;
            sportIconBg = R.drawable.round_corner_layout_handball;
        } else if(sportCode == Integer.parseInt(context.getString(R.string.sport_code_peteca))){
            sportIcon = R.drawable.ic_peteca;
            sportIconBg = R.drawable.round_corner_layout_peteca;
        } else if(sportCode == Integer.parseInt(context.getString(R.string.sport_code_tennis))){
            sportIcon = R.drawable.ic_tennis_ball;
            sportIconBg = R.drawable.round_corner_layout_tenis;
        } else if(sportCode == Integer.parseInt(context.getString(R.string.sport_code_paintball))){
            sportIcon = R.drawable.ic_paintaball;
            sportIconBg = R.drawable.round_corner_layout_paintball;
        } else if(sportCode == Integer.parseInt(context.getString(R.string.sport_code_baseball))){
            sportIcon = R.drawable.ic_baseball;
            sportIconBg = R.drawable.round_corner_layout_baseball;
        } else if(sportCode == Integer.parseInt(context.getString(R.string.sport_code_golf))){
            sportIcon = R.drawable.ic_golf;
            sportIconBg = R.drawable.round_corner_layout_golf;
        } else if(sportCode == Integer.parseInt(context.getString(R.string.sport_code_american))){
            sportIcon = R.drawable.ic_americanfut;
            sportIconBg = R.drawable.round_corner_layout_americano;
        } else if(sportCode == Integer.parseInt(context.getString(R.string.sport_code_rugby))){
            sportIcon = R.drawable.ic_rugby;
            sportIconBg = R.drawable.round_corner_layout_rugby;
        } else if(sportCode == Integer.parseInt(context.getString(R.string.sport_code_other))){
            sportIcon = R.drawable.ic_plus;
            sportIconBg = R.drawable.round_corner_layout_other;
        } else {
            sportIcon = R.drawable.ic_plus;
            sportIconBg = R.drawable.round_corner_layout_other;
        }

        ImageView imgSport = (ImageView) rootView.findViewById(R.id.imgSport);
        imgSport.setImageResource(sportIcon);
        imgSport.setBackgroundResource(sportIconBg);

        return rootView;
    }
}
