package com.kreativ.sportmatch.fragments;


import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.kreativ.sportmatch.R;


public class LoginSignUpFragment extends Fragment {

    private EditText etxtUsername, etxtEmail, etxtName, etxtPassword;

    public LoginSignUpFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_sign_in, container, false);

        etxtUsername = (EditText) view.findViewById(R.id.etxtUsername);
        etxtPassword = (EditText) view.findViewById(R.id.etxtPassword);
        etxtEmail = (EditText) view.findViewById(R.id.etxtEmail);
        etxtName = (EditText) view.findViewById(R.id.etxtName);

        Button btnSignUp = (Button) view.findViewById(R.id.btnSignUp);
        Button btnLogin = (Button) view.findViewById(R.id.btnLogin);

        // Esconde botao de criar conta
        btnSignUp.setVisibility(View.GONE);
        btnLogin.setText(getString(R.string.action_sign_up_and_enter));

        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String user = etxtUsername.getText().toString();
                String password = etxtPassword.getText().toString();
                String email = etxtEmail.getText().toString();
                String name = etxtName.getText().toString();

                FirebaseAuth mAuth = FirebaseAuth.getInstance();
                mAuth.createUserWithEmailAndPassword(user, password)
                        .addOnCompleteListener(getActivity(), new OnCompleteListener<AuthResult>() {
                            @Override
                            public void onComplete(@NonNull Task<AuthResult> task) {
                                if (!task.isSuccessful()) {
                                    Toast.makeText(getContext(), "Authentication failed.", Toast.LENGTH_SHORT).show();
                                }
                            }
                        });
            }
        });

        return view;
    }
}
