package com.kreativ.sportmatch.activitys;

import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.backendless.Backendless;
import com.backendless.BackendlessCollection;
import com.backendless.BackendlessUser;
import com.backendless.async.callback.AsyncCallback;
import com.backendless.exceptions.BackendlessFault;
import com.backendless.persistence.BackendlessDataQuery;
import com.kreativ.sportmatch.R;
import com.kreativ.sportmatch.models.Match;
import com.kreativ.sportmatch.models.MatchPlayers;

public class InviteActivity extends AppCompatActivity {

    private LinearLayout userData, screenLoading;
    private EditText etxtUsername;
    private Button btnSearch;
    private Button btnInvite;
    private TextView txtUsername, txtName, txtEmail;
    private String username;
    private BackendlessUser user;
    private Bundle extras;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_invite);

        linkScreenElements();

        extras = getIntent().getExtras();

        btnInvite.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AsyncTask task = new AsyncTask() {
                    @Override
                    protected Object doInBackground(Object[] params) {
                        String whereClause = "objectId = '"+ extras.getString("exMatchId") +"'";
                        BackendlessDataQuery dataQuery = new BackendlessDataQuery();
                        dataQuery.setWhereClause(whereClause);
                        BackendlessCollection<Match> result = Backendless.Persistence.of(Match.class).find(dataQuery);

                        return result;
                    }

                    @Override
                    protected void onPostExecute(Object o) {
                        BackendlessCollection<Match> backColl = (BackendlessCollection<Match>) o;

                        MatchPlayers matchPlayers = new MatchPlayers();
                        matchPlayers.setUser(user);
                        matchPlayers.setMatch(backColl.getData().get(0));
                        matchPlayers.setStatus(4);

                        Backendless.Persistence.save(matchPlayers, new AsyncCallback<MatchPlayers>() {
                            @Override
                            public void handleResponse(MatchPlayers response) {
                                Log.e("INVITE_USER", "--- Usuário convidado");
                                Toast.makeText(InviteActivity.this, "Usuário convidado!", Toast.LENGTH_LONG).show();

                                userData.setVisibility(View.GONE);
                                btnInvite.setVisibility(View.GONE);
                            }

                            @Override
                            public void handleFault(BackendlessFault fault) {
                                Log.e("INVITE_USER", "--- Problema ao convidar o usuário");
                                Toast.makeText(InviteActivity.this, "Usuário convidado!", Toast.LENGTH_LONG).show();
                            }
                        });
                    }
                };
                task.execute();
            }
        });

        btnSearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                screenLoading.setVisibility(View.VISIBLE);
                username = etxtUsername.getText().toString();
                AsyncTask task = new AsyncTask() {
                    @Override
                    protected Object doInBackground(Object[] params) {
                        String whereClause = "username like '%"+ username +"%'";
                        BackendlessDataQuery dataQuery = new BackendlessDataQuery();
                        dataQuery.setWhereClause(whereClause);
                        BackendlessCollection<BackendlessUser> result = Backendless.Persistence.of(BackendlessUser.class).find(dataQuery);

                        return result;
                    }

                    @Override
                    protected void onPostExecute(Object o) {
                        BackendlessCollection<BackendlessUser> backColl = (BackendlessCollection<BackendlessUser>) o;

                        if (backColl.getData().size() != 0) {
                            user = backColl.getData().get(0);
                            txtUsername.setText(user.getProperty("username").toString());
                            txtName.setText(user.getProperty("name").toString());
                            txtEmail.setText(user.getProperty("email").toString());

                            userData.setVisibility(View.VISIBLE);
                            btnInvite.setVisibility(View.VISIBLE);
                            btnInvite.setText(getString(R.string.btn_invite) + " " + user.getProperty("name").toString());
                        } else {
                            Toast.makeText(InviteActivity.this, "Usuário não encontrado!", Toast.LENGTH_LONG).show();
                            userData.setVisibility(View.GONE);
                            btnInvite.setVisibility(View.GONE);
                        }

                        screenLoading.setVisibility(View.GONE);
                    }
                };
                task.execute();
            }
        });
    }

    public void linkScreenElements() {
        userData = (LinearLayout) findViewById(R.id.userData);
        screenLoading = (LinearLayout) findViewById(R.id.screenLoading);
        etxtUsername = (EditText) findViewById(R.id.etxtUsername);
        btnSearch = (Button) findViewById(R.id.btnSearch);
        btnInvite = (Button) findViewById(R.id.btnInvite);
        txtUsername = (TextView) findViewById(R.id.txtUsername);
        txtName = (TextView) findViewById(R.id.txtName);
        txtEmail = (TextView) findViewById(R.id.txtEmail);
    }
}






