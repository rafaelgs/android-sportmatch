package com.kreativ.sportmatch.fragments;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Toast;

import com.kreativ.sportmatch.R;

public class LoginMainFragment extends Fragment {

    public LoginMainFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_login, container, false);

        Button btnLoginFacebook = (Button) view.findViewById(R.id.btnLoginFacebook);
        Button btnLoginGoogle = (Button) view.findViewById(R.id.btnLoginGoogle);
        Button btnLoginEmail = (Button) view.findViewById(R.id.btnLoginEmail);

        btnLoginFacebook.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(getContext(), "Em desenvolvimento...", Toast.LENGTH_SHORT).show();
            }
        });

        btnLoginGoogle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(getContext(), "Em desenvolvimento...", Toast.LENGTH_SHORT).show();
            }
        });

        btnLoginEmail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Troca o fragmento
                getActivity()
                        .getSupportFragmentManager()
                        .beginTransaction()
                        .replace(R.id.container, new LoginSignInFragment())
                        .addToBackStack(null)
                        .commit();
            }
        });

        return view;
    }

}
