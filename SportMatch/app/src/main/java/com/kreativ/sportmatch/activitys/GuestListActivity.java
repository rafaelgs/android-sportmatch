package com.kreativ.sportmatch.activitys;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import android.widget.LinearLayout;
import android.widget.ListView;

import com.backendless.Backendless;
import com.backendless.BackendlessCollection;
import com.backendless.persistence.BackendlessDataQuery;
import com.kreativ.sportmatch.R;
import com.kreativ.sportmatch.adapters.GuestListAdapter;
import com.kreativ.sportmatch.models.MatchPlayers;

import java.util.List;

public class GuestListActivity extends AppCompatActivity {

    /**
     * The {@link android.support.v4.view.PagerAdapter} that will provide
     * fragments for each of the sections. We use a
     * {@link FragmentPagerAdapter} derivative, which will keep every
     * loaded fragment in memory. If this becomes too memory intensive, it
     * may be best to switch to a
     * {@link android.support.v4.app.FragmentStatePagerAdapter}.
     */
    private SectionsPagerAdapter mSectionsPagerAdapter;

    /**
     * The {@link ViewPager} that will host the section contents.
     */
    private ViewPager mViewPager;
    private static Bundle extras;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_guest_list);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        // Create the adapter that will return a fragment for each of the three
        // primary sections of the activity.
        mSectionsPagerAdapter = new SectionsPagerAdapter(getSupportFragmentManager());

        // Set up the ViewPager with the sections adapter.
        mViewPager = (ViewPager) findViewById(R.id.container);
        mViewPager.setAdapter(mSectionsPagerAdapter);

        TabLayout tabLayout = (TabLayout) findViewById(R.id.tabs);
        tabLayout.setupWithViewPager(mViewPager);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(GuestListActivity.this, InviteActivity.class);
                startActivity(intent);
            }
        });

        // Define o extras
        extras = getIntent().getExtras();
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_guest_list, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    /**
     * A placeholder fragment containing a simple view.
     */
    public static class PlaceholderFragment extends Fragment {
        /**
         * The fragment argument representing the section number for this
         * fragment.
         */
        private static final String ARG_SECTION_NUMBER = "section_number";

        private List<MatchPlayers> items1 = null, items2 = null, items3 = null, items4 = null;
        private ListView listMatches;
        private String whereClause;
        private int status;
        private LinearLayout screenLoading;
        private LinearLayout listGroup;

        public PlaceholderFragment() {
        }

        /**
         * Returns a new instance of this fragment for the given section
         * number.
         */
        public static PlaceholderFragment newInstance(int sectionNumber) {
            PlaceholderFragment fragment = new PlaceholderFragment();
            Bundle args = new Bundle();
            args.putInt(ARG_SECTION_NUMBER, sectionNumber);
            fragment.setArguments(args);
            return fragment;
        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                 Bundle savedInstanceState) {
            View rootView = inflater.inflate(R.layout.fragment_guest_list, container, false);

            listMatches = (ListView) rootView.findViewById(R.id.listMatches);
            screenLoading = (LinearLayout) rootView.findViewById(R.id.screenLoading);
            listGroup = (LinearLayout) rootView.findViewById(R.id.listGroup);

            listMatches.setEmptyView(rootView.findViewById(android.R.id.empty));

            if (getArguments().getInt(ARG_SECTION_NUMBER) == 1 && items1 == null) {
                whereClause = "match.objectId = '"+ extras.getString("exMatchId")+"' and status = 1";
                status = 1;
            } else if (getArguments().getInt(ARG_SECTION_NUMBER) == 2 && items2 == null) {
                whereClause = "match.objectId = '"+ extras.getString("exMatchId")+"' and status = 2";
                status = 2;
            } else if (getArguments().getInt(ARG_SECTION_NUMBER) == 3 && items3 == null) {
                whereClause = "match.objectId = '"+ extras.getString("exMatchId")+"' and status = 3";
                status = 3;
            } else if (getArguments().getInt(ARG_SECTION_NUMBER) == 4 && items4 == null) {
                whereClause = "match.objectId = '"+ extras.getString("exMatchId")+"' and status = 4";
                status = 4;
            }

            if (status == 1 && items1 != null) {
                GuestListAdapter adapter = new GuestListAdapter(getContext(), items1);
                listMatches.setAdapter(adapter);
            } else if (status == 2 && items2 != null) {
                GuestListAdapter adapter = new GuestListAdapter(getContext(), items2);
                listMatches.setAdapter(adapter);
            } else if (status == 3 && items3 != null) {
                GuestListAdapter adapter = new GuestListAdapter(getContext(), items3);
                listMatches.setAdapter(adapter);
            } else if (status == 4 && items4 != null) {
                GuestListAdapter adapter = new GuestListAdapter(getContext(), items4);
                listMatches.setAdapter(adapter);
            } else {
                listGroup.setVisibility(View.GONE);
                screenLoading.setVisibility(View.VISIBLE);

                AsyncTask task = new AsyncTask() {
                    @Override
                    protected Object doInBackground(Object[] params) {
                        BackendlessDataQuery dataQuery = new BackendlessDataQuery();
                        dataQuery.setWhereClause(whereClause);

                        BackendlessCollection<MatchPlayers> result = Backendless.Persistence.of(MatchPlayers.class).find(dataQuery);
                        return result;
                    }

                    @Override
                    protected void onPostExecute(Object o) {
                        BackendlessCollection<MatchPlayers> backColl = (BackendlessCollection<MatchPlayers>) o;

                        GuestListAdapter adapter = null;

                        if (status == 1) {
                            items1 = backColl.getData();
                            adapter = new GuestListAdapter(getContext(), items1);
                        } else if (status == 2) {
                            items2 = backColl.getData();
                            adapter = new GuestListAdapter(getContext(), items2);
                        } else if (status == 3) {
                            items3 = backColl.getData();
                            adapter = new GuestListAdapter(getContext(), items3);
                        } else {
                            items4 = backColl.getData();
                            adapter = new GuestListAdapter(getContext(), items4);
                        }

                        listMatches.setAdapter(adapter);

                        screenLoading.setVisibility(View.GONE);
                        listGroup.setVisibility(View.VISIBLE);
                    }
                };
                task.execute();
            }

            return rootView;
        }
    }

    /**
     * A {@link FragmentPagerAdapter} that returns a fragment corresponding to
     * one of the sections/tabs/pages.
     */
    public class SectionsPagerAdapter extends FragmentPagerAdapter {

        public SectionsPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            // getItem is called to instantiate the fragment for the given page.
            // Return a PlaceholderFragment (defined as a static inner class below).
            return PlaceholderFragment.newInstance(position + 1);
        }

        @Override
        public int getCount() {
            // Show 4 total pages.
            return 4;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            switch (position) {
                case 0:
                    return "VOU!";
                case 1:
                    return "NÃO VOU!";
                case 2:
                    return "TALVEZ";
                case 3:
                    return "CONV.";
            }
            return null;
        }


    }
}
