package com.kreativ.sportmatch.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.kreativ.sportmatch.R;
import com.kreativ.sportmatch.models.Match;

import java.util.Calendar;
import java.util.List;

/**
 * Created by Rafael on 03/03/2016.
 */
public class RecyclerListAdapter extends RecyclerView.Adapter<RecyclerListAdapter.ViewHolder> {
    private static final String TAG = "RecyclerListAdapter";

    private List<Match> mDataSet;
    private Context mContext;

    /**
     * Provide a reference to the type of views that you are using (custom ViewHolder)
     */
    public static class ViewHolder extends RecyclerView.ViewHolder{
        private final TextView txtMatchTitle, txtMatchDate;
        private final ImageView imgSport;
        private Match match;
        private Context context;

        public ViewHolder(View v) {
            super(v);

            v.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Log.d(TAG, "Element " + getPosition() + " clicked.");
                }
            });

            txtMatchTitle = (TextView) v.findViewById(R.id.txtMatchTitle);
            txtMatchDate = (TextView) v.findViewById(R.id.txtMatchDate);
            imgSport = (ImageView) v.findViewById(R.id.imgSport);
        }

        public TextView getTxtMatchTitle() {
            return txtMatchTitle;
        }

        public TextView getTxtMatchDate() {
            return txtMatchDate;
        }

        public ImageView getImgSport() {
            return imgSport;
        }
    }

    /**
     * Initialize the dataset of the Adapter.
     *
     * @param list String[] containing the data to populate views to be used by RecyclerView.
     */
    public RecyclerListAdapter(Context context, List<Match> list) {
        mDataSet = list;
        mContext = context;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
        // Create a new view.
        View v = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.list_item_match, viewGroup, false);

        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(ViewHolder viewHolder, final int position) {
        Log.d(TAG, "Element " + position + " set.");

        // Define o nome
        viewHolder.getTxtMatchTitle().setText(mDataSet.get(position).getName());

        // Define a data
        Calendar cal = Calendar.getInstance();
        cal.setTime(mDataSet.get(position).getDateStart());

        String date = "";

        if(cal.get(Calendar.DAY_OF_MONTH) < 10){
            date += "0" + (cal.get(Calendar.DAY_OF_MONTH) + 1);
        } else {
            date += (cal.get(Calendar.DAY_OF_MONTH) + 1);
        }

        date += "/";

        if(cal.get(Calendar.MONTH) < 10){
            date += "0" + cal.get(Calendar.MONTH);
        } else {
            date += cal.get(Calendar.MONTH);
        }

        viewHolder.getTxtMatchDate().setText(date);

        // Define o esporte
        int sportCode = mDataSet.get(position).getSport();
        int sportIcon = 0;
        int sportIconBg = 0;
        if(sportCode == Integer.parseInt(mContext.getString(R.string.sport_code_futsal))){
            sportIcon = R.drawable.ic_futsal;
            sportIconBg = R.drawable.round_corner_layout_futsal;
        } else if(sportCode == Integer.parseInt(mContext.getString(R.string.sport_code_society))){
            sportIcon = R.drawable.ic_fut_society;
            sportIconBg = R.drawable.round_corner_layout_society;
        } else if(sportCode == Integer.parseInt(mContext.getString(R.string.sport_code_soccer))){
            sportIcon = R.drawable.ic_soccer;
            sportIconBg = R.drawable.round_corner_layout_soccer;
        } else if(sportCode == Integer.parseInt(mContext.getString(R.string.sport_code_volley))) {
            sportIcon = R.drawable.ic_volei;
            sportIconBg = R.drawable.round_corner_layout_volley;
        } else if(sportCode == Integer.parseInt(mContext.getString(R.string.sport_code_basket))){
            sportIcon = R.drawable.ic_basquete;
            sportIconBg = R.drawable.round_corner_layout_basquete;
        } else if(sportCode == Integer.parseInt(mContext.getString(R.string.sport_code_handball))){
            sportIcon = R.drawable.ic_handball;
            sportIconBg = R.drawable.round_corner_layout_handball;
        } else if(sportCode == Integer.parseInt(mContext.getString(R.string.sport_code_peteca))){
            sportIcon = R.drawable.ic_peteca;
            sportIconBg = R.drawable.round_corner_layout_peteca;
        } else if(sportCode == Integer.parseInt(mContext.getString(R.string.sport_code_tennis))){
            sportIcon = R.drawable.ic_tennis_ball;
            sportIconBg = R.drawable.round_corner_layout_tenis;
        } else if(sportCode == Integer.parseInt(mContext.getString(R.string.sport_code_paintball))){
            sportIcon = R.drawable.ic_paintaball;
            sportIconBg = R.drawable.round_corner_layout_paintball;
        } else if(sportCode == Integer.parseInt(mContext.getString(R.string.sport_code_baseball))){
            sportIcon = R.drawable.ic_baseball;
            sportIconBg = R.drawable.round_corner_layout_baseball;
        } else if(sportCode == Integer.parseInt(mContext.getString(R.string.sport_code_golf))){
            sportIcon = R.drawable.ic_golf;
            sportIconBg = R.drawable.round_corner_layout_golf;
        } else if(sportCode == Integer.parseInt(mContext.getString(R.string.sport_code_american))){
            sportIcon = R.drawable.ic_americanfut;
            sportIconBg = R.drawable.round_corner_layout_americano;
        } else if(sportCode == Integer.parseInt(mContext.getString(R.string.sport_code_rugby))){
            sportIcon = R.drawable.ic_rugby;
            sportIconBg = R.drawable.round_corner_layout_rugby;
        } else if(sportCode == Integer.parseInt(mContext.getString(R.string.sport_code_other))){
            sportIcon = R.drawable.ic_plus;
            sportIconBg = R.drawable.round_corner_layout_other;
        } else {
            sportIcon = R.drawable.ic_plus;
            sportIconBg = R.drawable.round_corner_layout_other;
        }

        viewHolder.getImgSport().setImageResource(sportIcon);
        viewHolder.getImgSport().setBackgroundResource(sportIconBg);
    }

    @Override
    public int getItemCount() {
        return mDataSet.size();
    }
}
