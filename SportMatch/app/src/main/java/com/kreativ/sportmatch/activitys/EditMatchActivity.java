package com.kreativ.sportmatch.activitys;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.location.Address;
import android.location.Geocoder;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Switch;
import android.widget.TimePicker;
import android.widget.Toast;

import com.backendless.Backendless;
import com.backendless.BackendlessCollection;
import com.backendless.async.callback.AsyncCallback;
import com.backendless.exceptions.BackendlessFault;
import com.backendless.geo.GeoPoint;
import com.backendless.persistence.BackendlessDataQuery;
import com.kreativ.sportmatch.R;
import com.kreativ.sportmatch.models.Match;
import com.kreativ.sportmatch.models.MatchPlayers;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

public class EditMatchActivity extends AppCompatActivity {

    private EditText etxtTitle, etxtAddress, etxtDate, etxtHourBegin, etxtHourEnd, etxtDesc, etxtMaxUnknown ;
    private Switch swPublic;
    private Button btnSaveMatch, btnDeleteMatch, btnSportSelected;
    private int exSport, exNumMax;
    Date dateStart = new Date(), dateEnd = new Date();
    private EditText otherSport;
    private String exName, exLocal,  exDesc ;
    private Date exDate;
    private Boolean exOpen;
    private Bundle extras;
    private boolean success, editSport;
    Spinner spinnerSports;

    Date date = new Date();
    int numMax = 0;
    String exHourBegin = "";
    String exHourEnd = "";




    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_match);

        linkScreenElements();

        extras = getIntent().getExtras();
        exSport = getIntent().getExtras().getInt("exSport");

        if(extras.getString("exName") !=  null){
            fillMatchWithExtras();
        }

        int text = 0;
        int bgColor = 0;
        int drawable = 0;

        btnSportSelected = (Button) findViewById(R.id.btnSportSelected);
        if (exSport == Integer.parseInt(getString(R.string.sport_code_futsal))) {
            text = R.string.btn_futsal;
            bgColor = R.color.btnFutsal;
            drawable = R.drawable.ic_futsal;
        } else if (exSport == Integer.parseInt(getString(R.string.sport_code_society))) {
            text = R.string.btn_fut_society;
            bgColor = R.color.btnSociety;
            drawable = R.drawable.ic_fut_society;
        } else if (exSport == Integer.parseInt(getString(R.string.sport_code_soccer))) {
            text = R.string.btn_soccer;
            bgColor = R.color.btnFutebol;
            drawable = R.drawable.ic_soccer;
        } else if (exSport == Integer.parseInt(getString(R.string.sport_code_volley))) {
            text = R.string.btn_volei;
            bgColor = R.color.btnVolei;
            drawable = R.drawable.ic_volei;
        } else if (exSport == Integer.parseInt(getString(R.string.sport_code_basket))) {
            text = R.string.btn_basquete;
            bgColor = R.color.btnBasquete;
            drawable = R.drawable.ic_basquete;
        } else if (exSport == Integer.parseInt(getString(R.string.sport_code_handball))) {
            text = R.string.btn_handball;
            bgColor = R.color.btnHandball;
            drawable = R.drawable.ic_handball;
        } else if (exSport == Integer.parseInt(getString(R.string.sport_code_peteca))) {
            text = R.string.btn_peteca;
            bgColor = R.color.btnPeteca;
            drawable = R.drawable.ic_peteca;
        } else if (exSport == Integer.parseInt(getString(R.string.sport_code_tennis))) {
            text = R.string.btn_tenis;
            bgColor = R.color.btnTenis;
            drawable = R.drawable.ic_tennis_ball;
        } else if (exSport == Integer.parseInt(getString(R.string.sport_code_paintball))) {
            text = R.string.btn_paintball;
            bgColor = R.color.btnPaintball;
            drawable = R.drawable.ic_paintaball;
        } else if (exSport == Integer.parseInt(getString(R.string.sport_code_baseball))) {
            text = R.string.btn_basebal;
            bgColor = R.color.btnBaseball;
            drawable = R.drawable.ic_baseball;
        } else if (exSport == Integer.parseInt(getString(R.string.sport_code_golf))) {
            text = R.string.btn_golf;
            bgColor = R.color.btnGolf;
            drawable = R.drawable.ic_golf;
        } else if (exSport == Integer.parseInt(getString(R.string.sport_code_american))) {
            text = R.string.btn_american_fut;
            bgColor = R.color.btnAmericano;
            drawable = R.drawable.ic_americanfut;
        } else if (exSport == Integer.parseInt(getString(R.string.sport_code_rugby))) {
            text = R.string.btn_rugby;
            bgColor = R.color.btnRugby;
            drawable = R.drawable.ic_rugby;
        } else if (exSport == Integer.parseInt(getString(R.string.sport_code_other))) {
            text = R.string.btn_outro;
            bgColor = R.color.btnOthers;
            drawable = R.drawable.ic_plus;
            otherSport.setVisibility(View.VISIBLE);
        }

        if (text != 0) {
            btnSportSelected.setText(getResources().getString(text));
            btnSportSelected.setBackgroundColor(getResources().getColor(bgColor));
            btnSportSelected.setCompoundDrawablesWithIntrinsicBounds(getResources().getDrawable(drawable), null, null, null);
        }

        getSupportActionBar().setTitle(getResources().getString(R.string.title_edit_match));

    }//create

    //valida os campos da tela
    public boolean validateMatch(){
        boolean success = true;

        if (etxtTitle.getText().toString().trim().equalsIgnoreCase("")){
           etxtTitle.setError(getString(R.string.error_empty_field));
           success = false;
        }
        if (etxtAddress.getText().toString().trim().equalsIgnoreCase("")){
            etxtAddress.setError(getString(R.string.error_empty_field));
            success = false;
        }
        if (etxtDate.getText().toString().trim().equalsIgnoreCase("")){
            etxtDate.setError(getString(R.string.error_empty_field));
            success = false;
        }
        if (etxtHourBegin.getText().toString().trim().equalsIgnoreCase("")){
            etxtHourBegin.setError(getString(R.string.error_empty_field));
            success = false;
        }


        return  success;
    }

    //preenche com extras
    public void fillMatchWithExtras(){
        exName = getIntent().getExtras().getString("exName");
        exLocal = getIntent().getExtras().getString("exLocal");
        exDate = (Date) getIntent().getSerializableExtra("exDate");
        exDesc = getIntent().getExtras().getString("exDesc");
        exHourBegin = getIntent().getExtras().getString("exHourBegin");
        exHourEnd = getIntent().getExtras().getString("exHourEnd");
        exNumMax = getIntent().getExtras().getInt("exMaxUnknown");
        exOpen = getIntent().getExtras().getBoolean("exOpen");
        editSport = getIntent().getExtras().getBoolean("editSport");


        spinnerSports = (Spinner) findViewById(R.id.editSport);
        btnSportSelected = (Button) findViewById(R.id.btnSportSelected);

        if(editSport){
            btnSportSelected.setVisibility(View.GONE);
            spinnerSports.setVisibility(View.VISIBLE);
            ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this,
                    R.array.sports, android.R.layout.simple_spinner_item);
            adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            spinnerSports.setAdapter(adapter);

        }

        etxtTitle.setText(exName);
        etxtAddress.setText(exLocal);
        etxtHourBegin.setText(exHourBegin);
        etxtHourEnd.setText(exHourEnd);
        etxtDesc.setText(exDesc);
        swPublic.setChecked(exOpen);

        Calendar cal = Calendar.getInstance();
        cal.setTime(exDate);

        String date = "";

        if(cal.get(Calendar.DAY_OF_MONTH) < 10){
            date += "0" + cal.get(Calendar.DAY_OF_MONTH);
        } else {
            date += cal.get(Calendar.DAY_OF_MONTH);
        }

        date += "/";

        if(cal.get(Calendar.MONTH) < 10){
            date += "0" + (cal.get(Calendar.MONTH) + 1);
        } else {
            date += (cal.get(Calendar.MONTH) + 1);
        }

        etxtDate.setText(date);

        btnDeleteMatch.setVisibility(View.VISIBLE);
    }

    //date picker
    @TargetApi(Build.VERSION_CODES.HONEYCOMB)
    @SuppressLint("NewApi")
    public void showDateDialog(){

        Calendar cal = Calendar.getInstance();

		DatePickerDialog dateDialog = new DatePickerDialog(this, new DatePickerDialog.OnDateSetListener() {
            public void onDateSet(DatePicker view, int selectedYear, int selectedMonth, int selectedDay) {

                Calendar cal = Calendar.getInstance();
                cal.setTime(dateStart);
                cal.set(selectedYear , selectedMonth, selectedDay);

                dateStart = cal.getTime();
                dateEnd = cal.getTime();

                String date = "";

                if(selectedDay < 10){
                    date += "0" + selectedDay;
                } else {
                    date += selectedDay;
                }

                date += "/";

                if(selectedMonth < 10){
                    date += "0" + (selectedMonth + 1);
                } else {
                    date += (selectedMonth + 1);
                }

                // set selected date into textview
                etxtDate.setText(date);
            }
        },cal.get(Calendar.YEAR), cal.get(Calendar.MONTH), cal.get(Calendar.DAY_OF_MONTH));

        //dateDialog.getDatePicker().setCalendarViewShown(false);
        //dateDialog.getDatePicker().setSpinnersShown(true);

        dateDialog.show();
    }

    //time picker
    public void showTimeDialog(final boolean hourBegin) {
        Calendar time = Calendar.getInstance();

        TimePickerDialog timePicker  = new TimePickerDialog(this, new TimePickerDialog.OnTimeSetListener() {
            public void onTimeSet(TimePicker timePicker, int selectedHour, int selectedMinute) {
                Calendar cal = Calendar.getInstance();
                if(hourBegin){
                    cal.setTime(dateStart);
                } else {
                    cal.setTime(dateEnd);
                }

                cal.set(Calendar.HOUR_OF_DAY, selectedHour);
                cal.set(Calendar.MINUTE, selectedMinute);

                if(hourBegin){
                    dateStart = cal.getTime();
                } else {
                    dateEnd = cal.getTime();
                }

                String date = "";

                if(selectedHour < 10) {
                   date += "0" + selectedHour;
                } else {
                   date += selectedHour;
                }

                date += ":";

                if(selectedMinute < 10) {
                   date += "0" + selectedMinute;
                } else {
                   date += selectedMinute;
                }

                if(hourBegin){
                   etxtHourBegin.setText(date);
                } else {
                   etxtHourEnd.setText(date);
                }

            }
        }, time.get(Calendar.HOUR) , time.get(Calendar.MINUTE), true);
        timePicker.show();
    }


    public void linkScreenElements() {
        etxtTitle = (EditText) findViewById(R.id.etxtTitle);
        etxtAddress = (EditText) findViewById(R.id.etxtAddress);
        etxtDate = (EditText) findViewById(R.id.etxtDate);
        etxtHourBegin = (EditText) findViewById(R.id.etxthourBegin);
        etxtHourEnd = (EditText) findViewById(R.id.etxtHourEnd);
        etxtDesc = (EditText) findViewById(R.id.etxtDesc);
        etxtMaxUnknown = (EditText) findViewById(R.id.etxtMaxUnknown);
        swPublic = (Switch) findViewById(R.id.swPublic);
        btnSaveMatch = (Button) findViewById(R.id.btnSaveMatch);
        otherSport = (EditText) findViewById(R.id.etxtSport);
        btnDeleteMatch = (Button) findViewById(R.id.btnDeleteMatch);
        spinnerSports = (Spinner) findViewById(R.id.editSport);

        btnSaveMatch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
            if(validateMatch()){
                saveMatch();
            } else {
                Toast.makeText(EditMatchActivity.this, "Preencha os campos corretamente", Toast.LENGTH_LONG).show();
            }

            }
        });

        swPublic.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
            if (etxtMaxUnknown.getVisibility() == View.GONE) {
                etxtMaxUnknown.setVisibility(View.VISIBLE);
            } else {
                etxtMaxUnknown.setVisibility(View.GONE);
            }

            }
        });

        etxtDate.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
            if (hasFocus) {
                showDateDialog();
            }
            }
        });

        etxtHourBegin.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
            if (hasFocus) {
                showTimeDialog(true);
            }
            }
        });

        etxtHourEnd.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (hasFocus) {
                    showTimeDialog(false);
                }
            }
        });

        btnDeleteMatch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new AlertDialog.Builder(EditMatchActivity.this)
                        .setTitle(getString(R.string.title_alert_delete_match))
                        .setMessage(getString(R.string.action_alert_delete_match))
                        .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                // Busca a partida e depois a deleta
                                AsyncTask taskDeleteMatchPlayer = new AsyncTask() {
                                    @Override
                                    protected Object doInBackground(Object[] params) {
                                        String whereClause = "match.objectId = '" + extras.getString("exMatchId") + "'";

                                        BackendlessDataQuery dataQuery = new BackendlessDataQuery();
                                        dataQuery.setWhereClause(whereClause);

                                        BackendlessCollection<MatchPlayers> result = Backendless.Persistence.of(MatchPlayers.class).find(dataQuery);

                                        return result;
                                    }

                                    @Override
                                    protected void onPostExecute(Object o) {
                                        BackendlessCollection<MatchPlayers> backColl = (BackendlessCollection<MatchPlayers>) o;

                                        if (backColl.getData().get(0) != null) {
                                            Backendless.Persistence.of(MatchPlayers.class).remove(backColl.getData().get(0), new AsyncCallback<Long>() {
                                                @Override
                                                public void handleResponse(Long response) {
                                                    Log.e("DELETE_MATCH_PLAYER", "---- DELETADO COM SUCESSO");
                                                }

                                                @Override
                                                public void handleFault(BackendlessFault fault) {
                                                    Log.e("DELETE_MATCH_PLAYER", "---- ERRO AO DELETAR");
                                                }
                                            });
                                        }
                                    }
                                };
                                taskDeleteMatchPlayer.execute();

                                // Busca a partida e depois a deleta
                                AsyncTask taskDeleteMatch = new AsyncTask() {
                                    @Override
                                    protected Object doInBackground(Object[] params) {
                                        String whereClause = "objectId = '" + extras.getString("exMatchId") + "'";

                                        BackendlessDataQuery dataQuery = new BackendlessDataQuery();
                                        dataQuery.setWhereClause(whereClause);

                                        BackendlessCollection<Match> result = Backendless.Persistence.of(Match.class).find(dataQuery);

                                        return result;
                                    }

                                    @Override
                                    protected void onPostExecute(Object o) {
                                        BackendlessCollection<Match> backColl = (BackendlessCollection<Match>) o;

                                        if (backColl.getData().get(0) != null) {
                                            Backendless.Persistence.of(Match.class).remove(backColl.getData().get(0), new AsyncCallback<Long>() {
                                                @Override
                                                public void handleResponse(Long response) {
                                                    Log.e("DELETE_MATCH", "---- DELETADO COM SUCESSO");
                                                    Toast.makeText(getApplicationContext(), getString(R.string.success_delete_match), Toast.LENGTH_LONG).show();

                                                    Intent intent = new Intent(EditMatchActivity.this, MainActivity.class);
                                                    startActivity(intent);
                                                }

                                                @Override
                                                public void handleFault(BackendlessFault fault) {
                                                    Log.e("DELETE_MATCH", "---- ERRO AO DELETAR");
                                                    Toast.makeText(getApplicationContext(), getString(R.string.error_delete_match), Toast.LENGTH_LONG).show();
                                                }
                                            });
                                        }
                                    }
                                };
                                taskDeleteMatch.execute();
                            }
                        })
                        .setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                // Nao faz nada
                            }
                        })
                        .show();
            }
        });
    }


    public void saveMatch() {
        SimpleDateFormat formatter = new SimpleDateFormat("dd/MM");

        try {
             date = formatter.parse(etxtDate.getText().toString());
        } catch (Exception e) {
            Log.e("SAVE_MATCH", "Erro ao converter a data - " + e.getMessage());
        }

        if(!etxtMaxUnknown.getText().toString().equals("")){
            numMax = Integer.parseInt(etxtMaxUnknown.getText().toString());
        }

        final Match match = new Match();
        match.setSport(exSport);
        match.setName(etxtTitle.getText().toString());
        match.setLocal(etxtAddress.getText().toString());
        match.setDateStart(dateStart);
        match.setDateEnd(dateEnd);
        match.setDescription(etxtDesc.getText().toString());
        match.setOpen(swPublic.isChecked());
        match.setMaxUnknown(numMax);
        match.setOwner(Backendless.UserService.CurrentUser());

        // Converte o endereço em GeoPoint
        success = true;
        final GeoPoint geopoint = getLocationFromAddress(etxtAddress.getText().toString());
        Backendless.Geo.savePoint(geopoint, new AsyncCallback<GeoPoint>() {
            @Override
            public void handleResponse(GeoPoint response) {
                Log.e("SAVE_MATCH", "LOCAL SALVO COM SUCESSO");

                match.setGeopoint(geopoint);
                Backendless.Persistence.save(match, new AsyncCallback<Match>() {
                    public void handleResponse(Match response) {
                        // Coloca o organizador na lista de convidados da partida
                        MatchPlayers matchPlayers = new MatchPlayers();
                        matchPlayers.setMatch(match);
                        matchPlayers.setUser(Backendless.UserService.CurrentUser());
                        matchPlayers.setStatus(Integer.parseInt(getString(R.string.status_code_confirmed)));
                        Backendless.Persistence.save(matchPlayers, new AsyncCallback<MatchPlayers>() {
                            @Override
                            public void handleResponse(MatchPlayers response) {
                                Log.e("SAVE_MATCH", "--- Criador colocado na lista");
                            }
                            @Override
                            public void handleFault(BackendlessFault fault) {
                                Log.e("SAVE_MATCH", "--- Erro ao colocar o criador na lista / " + fault.getMessage());
                            }
                        });

                        // Prepara os extras para a proxima tela
                        Intent intent = new Intent(EditMatchActivity.this, MatchDetailActivity.class);
                        intent.putExtra("exSport", exSport);
                        intent.putExtra("exName", etxtTitle.getText().toString());
                        intent.putExtra("exLocal", etxtAddress.getText().toString());
                        intent.putExtra("exDate", date);
                        intent.putExtra("exHourBegin", etxtHourBegin.getText().toString());
                        intent.putExtra("exHourEnd", etxtHourEnd.getText().toString());
                        intent.putExtra("exDesc", etxtDesc.getText().toString());
                        intent.putExtra("exOpen", swPublic.isChecked());
                        intent.putExtra("exMaxUnknown", numMax);
                        intent.putExtra("exMatchId", response.getObjectId());
                        startActivity(intent);
                        finish();
                    }

                    public void handleFault(BackendlessFault fault) {
                        Log.e("SAVE_MATCH", "--- Erro ao colocar o criador na lista / " + fault.getMessage());
                    }
                });
            }
            @Override
            public void handleFault(BackendlessFault fault) {
                Log.e("SAVE_MATCH", "ERRO AO SALVAR O LOCAL");

                etxtAddress.setError(getString(R.string.error_incorrect_address));
            }
        });
    }

    public GeoPoint getLocationFromAddress(String strAddress){

        Geocoder coder = new Geocoder(this);
        List<Address> address;
        GeoPoint p1 = null;

        try {
            address = coder.getFromLocationName(strAddress,5);
            if (address == null) {
                return null;
            }
            Address location=address.get(0);
            location.getLatitude();
            location.getLongitude();

            p1 = new GeoPoint((int) (location.getLatitude() * 1E6),
                    (int) (location.getLongitude() * 1E6));
        } catch (Exception e) {
            Toast.makeText(this, "Problema ao converter o endereco", Toast.LENGTH_SHORT).show();
        }

        return p1;
    }

    @Override
    public void onBackPressed() {
        finish();
    }
}
