package com.kreativ.sportmatch.activitys;

import android.content.Intent;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.SeekBar;
import android.widget.Spinner;
import android.widget.TextView;

import com.backendless.Backendless;
import com.backendless.BackendlessCollection;
import com.backendless.persistence.BackendlessDataQuery;
import com.kreativ.sportmatch.R;
import com.kreativ.sportmatch.adapters.MatchListAdapter;
import com.kreativ.sportmatch.models.Match;

import java.util.ArrayList;
import java.util.List;

public class SearchMatchActivity extends AppCompatActivity {

    private ListView listMatchesFound;
    private ArrayAdapter<String> adapter;
    private TextView txtRadius;
    private int radius;
    private SeekBar seekBar;
    private Spinner spinnerSports;
    private Button btnSearch;
    private List<Match> matchList;
    private int sportSelected = 1;
    private LinearLayout screenLoading, screenElements;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search_matchs);

        linkScreenElements();

        listMatchesFound.setEmptyView(findViewById(android.R.id.empty));
        listMatchesFound.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent intent = new Intent(SearchMatchActivity.this, MatchDetailActivity.class);
                intent.putExtra("exMatchId", matchList.get(position).getObjectId());
                intent.putExtra("exMatchName", matchList.get(position).getName());
                intent.putExtra("exSport", matchList.get(position).getSport());
                startActivity(intent);
            }
        });

        seekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                txtRadius.setText(String.valueOf(progress) + " km");
                radius = progress;
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });

        // Setup do spinner
        List<String> sportList = new ArrayList<>(14);
        sportList.add(getString(R.string.sport_futsal));
        sportList.add(getString(R.string.sport_society));
        sportList.add(getString(R.string.sport_soccer));
        sportList.add(getString(R.string.sport_volley));
        sportList.add(getString(R.string.sport_basket));
        sportList.add(getString(R.string.sport_handball));
        sportList.add(getString(R.string.sport_peteca));
        sportList.add(getString(R.string.sport_tennis));
        sportList.add(getString(R.string.sport_paintball));
        sportList.add(getString(R.string.sport_baseball));
        sportList.add(getString(R.string.sport_golf));
        sportList.add(getString(R.string.sport_american));
        sportList.add(getString(R.string.sport_rugby));
        sportList.add(getString(R.string.sport_other));
        ArrayAdapter<String> adapterSpinner = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, sportList);
        spinnerSports.setAdapter(adapterSpinner);
        spinnerSports.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                sportSelected = position + 1;
                Log.e("SPORT_SELECTED", "Esporte selecionado: " +sportSelected);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                // Nada
            }
        });

        // Ação de busca
        btnSearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Mostra tela de carregamento
                screenLoading.setVisibility(View.VISIBLE);
                screenElements.setVisibility(View.GONE);

                AsyncTask task = new AsyncTask() {
                    @Override
                    protected Object doInBackground(Object[] params) {
                        String whereClause = "open = true and sport = "+ sportSelected;

                        BackendlessDataQuery dataQuery = new BackendlessDataQuery();
                        dataQuery.setWhereClause(whereClause);

                        BackendlessCollection<Match> result = Backendless.Persistence.of(Match.class).find(dataQuery);

                        return result;
                    }

                    @Override
                    protected void onPostExecute(Object o) {
                        BackendlessCollection<Match> backColl = (BackendlessCollection<Match>) o;
                        matchList = backColl.getData();

                        MatchListAdapter adapter = new MatchListAdapter(SearchMatchActivity.this, matchList);
                        listMatchesFound.setAdapter(adapter);

                        screenLoading.setVisibility(View.GONE);
                        screenElements.setVisibility(View.VISIBLE);
                    }
                };
                task.execute();
            }
        });
    }

    public void linkScreenElements() {
        txtRadius = (TextView) findViewById(R.id.txtRadius);
        seekBar = (SeekBar) findViewById(R.id.seekBarRadius);
        listMatchesFound = (ListView) findViewById(R.id.listMatchesFound);
        spinnerSports = (Spinner) findViewById(R.id.spinnerSportsSearch);
        btnSearch = (Button) findViewById(R.id.btnSearchMatches);
        screenLoading = (LinearLayout) findViewById(R.id.screenLoading);
        screenElements = (LinearLayout) findViewById(R.id.screenElements);
    }
}
