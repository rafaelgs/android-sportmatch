package com.kreativ.sportmatch.activitys;

import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.ListView;

import com.backendless.Backendless;
import com.backendless.BackendlessCollection;
import com.backendless.persistence.BackendlessDataQuery;
import com.backendless.persistence.local.UserIdStorageFactory;
import com.kreativ.sportmatch.R;
import com.kreativ.sportmatch.adapters.MatchPlayerListAdapter;
import com.kreativ.sportmatch.models.MatchPlayers;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link MainFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link MainFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class MainFragment extends Fragment {

    private ListView listNextMatchs;
    private LinearLayout listGroup;
    private LinearLayout listLoading;
    private BackendlessCollection<MatchPlayers> backColl;
    private ArrayList<MatchPlayers> nextDates;

    public MainFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @return A new instance of fragment MainFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static MainFragment newInstance(String param1, String param2) {
        MainFragment fragment = new MainFragment();
        Bundle args = new Bundle();

        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_main, container, false);

        ImageButton btnCreateMatch = (ImageButton) view.findViewById(R.id.btnNewMatch);
        btnCreateMatch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), SportSelectActivity.class);
                startActivity(intent);
            }
        });

        ImageButton btnSearchMatch = (ImageButton) view.findViewById(R.id.btnSearchMatch);
        btnSearchMatch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), SearchMatchActivity.class);
                startActivity(intent);
            }
        });

        listNextMatchs = (ListView) view.findViewById(R.id.listNextMatchs);
        listNextMatchs.setEmptyView(view.findViewById(android.R.id.empty));

        listGroup = (LinearLayout) view.findViewById(R.id.listGroup);
        listLoading = (LinearLayout) view.findViewById(R.id.listLoading);

        //Toast.makeText(getActivity(), ""+ UserIdStorageFactory.instance().getStorage().get(), Toast.LENGTH_SHORT).show();

        AsyncTask task = new AsyncTask() {
            @Override
            protected Object doInBackground(Object[] params) {
                String userId = UserIdStorageFactory.instance().getStorage().get();
                String whereClause = "user.objectId = '"+userId+"'";

//                QueryOptions queryOptions = new QueryOptions();
//                queryOptions.addSortByOption( "dateStart asc" );

                BackendlessDataQuery dataQuery = new BackendlessDataQuery();
                dataQuery.setWhereClause(whereClause);
//                dataQuery.setQueryOptions(queryOptions);

                BackendlessCollection<MatchPlayers> result = Backendless.Persistence.of(MatchPlayers.class).find(dataQuery);

                return result;
            }

            @Override
            protected void onPostExecute(Object o) {
                listGroup.setVisibility(View.VISIBLE);
                listLoading.setVisibility(View.GONE);

                backColl = (BackendlessCollection<MatchPlayers>) o;
                //fazer filtro das 3 proximas partidas
//                backColl.getData();Pega lista
//                backColl.getData().get(0);Primeiro item da lista
//                backColl.getData().get(0).getMatch();Acessa a Match dentro do MatchPlayers


                List<MatchPlayers> list = backColl.getData();
                nextDates = new ArrayList<>();//lista com as 3 proximas partidas
                java.util.Date currentDate = Calendar.getInstance().getTime();
                int cont = 0;
                for(int i = 0;i < list.size();i++){
                    //java.util.Date dataList = list.get(i).getMatch().getDateStart();//todas as partidas
                    MatchPlayers aux = list.get(i);
                    if(currentDate.before(list.get(i).getMatch().getDateStart()) || currentDate.equals(list.get(i).getMatch().getDateStart())) {
                        if (cont < 3)
                            nextDates.add(0, list.get(i));
                        else {
                            for (int j = 0; j < 3; j++) {
                                java.util.Date dataMatch = nextDates.get(0).getMatch().getDateStart();
                                if (dataMatch.after(aux.getMatch().getDateStart())){ //se data da lista for maior que uma das proximas
                                    nextDates.add(aux);
                                    aux = nextDates.get(0);
                                    nextDates.remove(0);
                                }
                            }

                        }cont++;
                    }
                }
//                java.util.Date aux;
//                for(int i=0;i<nextDates.size()-1;i++){
//                    aux = nextDates.get(i).getMatch().getDateStart();
//                    if(i == 0){
//                        if(aux.after(nextDates.get(i+1).getMatch().getDateStart())){
//                            nextDates.add(nextDates.get(i));
//                            nextDates.add(nextDates.get(1+2));
//                            nextDates.remove(i);
//                            nextDates.remove(i+2);
//                        }if(aux.after(nextDates.get(i+2).getMatch().getDateStart())){
//
//                        }
//                    }else{
//                        if(aux.after(nextDates.get(i+1).getMatch().getDateStart())) {
//                            nextDates.add(nextDates.get(i));
//                            nextDates.remove(i);
//                        }
//                    }
//                }


                MatchPlayerListAdapter adapter = new MatchPlayerListAdapter(getActivity(), nextDates);
                listNextMatchs.setAdapter(adapter);

                listNextMatchs.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                        MatchPlayers matchPlayers = nextDates.get(position);

                        Intent intent = new Intent(getContext(), MatchDetailActivity.class);
                        intent.putExtra("exMatchId", matchPlayers.getMatch().getObjectId());
                        intent.putExtra("exMatchName", matchPlayers.getMatch().getName());
                        intent.putExtra("exSport", matchPlayers.getMatch().getSport());
                        startActivity(intent);
                    }
                });
            }
        };
        task.execute();

        return view;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p/>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
}
