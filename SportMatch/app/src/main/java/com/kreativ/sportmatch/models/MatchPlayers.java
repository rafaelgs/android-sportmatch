package com.kreativ.sportmatch.models;

import com.backendless.BackendlessUser;

import java.util.Date;

/**
 * Created by Pacifico on 23/02/2016.
 */
public class MatchPlayers {
    private Match match;
    private int status;
    private BackendlessUser user;
    private String objectId;
    private Date created;
    private Date updated;

    public Match getMatch() {
        return match;
    }

    public void setMatch(Match match) {
        this.match = match;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public BackendlessUser getUser() {
        return user;
    }

    public void setUser(BackendlessUser user) {
        this.user = user;
    }

    public String getObjectId() {
        return objectId;
    }

    public void setObjectId(String objectId) {
        this.objectId = objectId;
    }

    public Date getCreated() {
        return created;
    }

    public void setCreated(Date created) {
        this.created = created;
    }

    public Date getUpdated() {
        return updated;
    }

    public void setUpdated(Date updated) {
        this.updated = updated;
    }


}
