package com.kreativ.sportmatch.models;

//import java.sql.Date;

import com.backendless.BackendlessUser;
import com.backendless.geo.GeoPoint;

import java.util.Date;

/**
 * Created by Pacifico on 19/02/2016.
 */
public class Match {
    private Date dateStart;
    private Date dateEnd;
    private String name;
    private int sport;
    private String objectId;
    private BackendlessUser owner;
    private Date created;
    private Date updated;
    private String description;
    private String local;
    private boolean open;
    private int maxUnknown;
    private GeoPoint geopoint;

    public GeoPoint getGeopoint() {
        return geopoint;
    }

    public void setGeopoint(GeoPoint geopoint) {
        this.geopoint = geopoint;
    }

    public int getMaxUnknown() {
        return maxUnknown;
    }

    public void setMaxUnknown(int maxUnknown) {
        this.maxUnknown = maxUnknown;
    }

    public Date getDateEnd() {
        return dateEnd;
    }

    public void setDateEnd(Date dateEnd) {
        this.dateEnd = dateEnd;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getLocal() {
        return local;
    }

    public void setLocal(String local) {
        this.local = local;
    }

    public boolean isOpen() {
        return open;
    }

    public void setOpen(boolean open) {
        this.open = open;
    }

    public Date getDateStart() {
        return dateStart;
    }

    public void setDateStart(Date dateStart) {
        this.dateStart = dateStart;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getSport() {
        return sport;
    }

    public void setSport( int sport ) {
        this.sport = sport;
    }

    public String getObjectId() {
        return objectId;
    }

    public void setObjectId( String objectId ) {
        this.objectId = objectId;
    }

    public BackendlessUser getOwner() {
        return owner;
    }

    public void setOwner(BackendlessUser owner) {
        this.owner = owner;
    }

    public Date getCreated() {
        return created;
    }

    public void setCreated( Date created ) {
        this.created = created;
    }

    public Date getUpdated() {
        return updated;
    }

    public void setUpdated(Date updated) {
        this.updated = updated;
    }
}
