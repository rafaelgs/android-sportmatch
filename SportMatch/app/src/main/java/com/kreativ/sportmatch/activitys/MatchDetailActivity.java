package com.kreativ.sportmatch.activitys;

import android.content.Intent;
import android.location.Address;
import android.location.Geocoder;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.backendless.Backendless;
import com.backendless.BackendlessCollection;
import com.backendless.BackendlessUser;
import com.backendless.async.callback.AsyncCallback;
import com.backendless.exceptions.BackendlessFault;
import com.backendless.persistence.BackendlessDataQuery;
import com.backendless.persistence.local.UserIdStorageFactory;
import com.kreativ.sportmatch.R;
import com.kreativ.sportmatch.models.Match;
import com.kreativ.sportmatch.models.MatchPlayers;

import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

public class MatchDetailActivity extends AppCompatActivity {

    private TextView txtAddress, txtDate, txtDesc, txtMaxUnknown, txtPrivate, txtSport , txtHourBegin, txtHourEnd , txtOwner;
    private Button btnEditMatch, btnGuestList , btnInvite;
    private Bundle extras;
    private Toolbar toolbar;
    private ImageButton imgOptionYes, imgOptionNo, imgOptionMaybe;
    private EditText etxtTitle;
    private int sportCode, exDateEnd, exMaxUnknown;
    private String exName, exLocal,  exDesc , exHourEnd , exHourBegin;
    private Boolean exOpen;
    private View toolbarLayout;
    private Date exDate;
    private Match match;
    private MatchPlayers matchPlayers;
    private FloatingActionButton floatEdit;
    private LinearLayout layoutTitleMaxUnknown, screenLoading, screenElements;
    private boolean editSport;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_match_detail);

        // Faz o link da tela com o java
        linkScreenElements();

        // Colore a barra de notificações
        Window window = getWindow();
        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
        window.setStatusBarColor(getResources().getColor(R.color.colorBlack));

        // Define o extras
        extras = getIntent().getExtras();

        // Seta o MatchPlayers para verificar status
        getMatchPlayers();

        // Diferencia a maneira de preencher os dados da tela de acordo com a tela que chamou a activity
        if(extras.getString("exName") == null) {
            fillMatchWithBD();
        } else {
            fillMatchWithExtras();
            screenLoading.setVisibility(View.GONE);
            screenElements.setVisibility(View.VISIBLE);
        }



    }//create

    public void linkScreenElements() {
        screenLoading = (LinearLayout) findViewById(R.id.screenLoading);
        screenElements = (LinearLayout) findViewById(R.id.screenElements);

        txtAddress = (TextView) findViewById(R.id.txtAddress);
        txtDate = (TextView) findViewById(R.id.txtDate);
        txtDesc = (TextView) findViewById(R.id.txtDesc);
        txtMaxUnknown = (TextView) findViewById(R.id.txtMaxUnknown);
        txtPrivate = (TextView) findViewById(R.id.txtPrivate);
        txtSport = (TextView) findViewById(R.id.txtSport);
        btnEditMatch = (Button) findViewById(R.id.btnSaveMatch);
        txtOwner = (TextView) findViewById(R.id.txtOwner);
        btnInvite = (Button) findViewById(R.id.btnInvite);

        layoutTitleMaxUnknown = (LinearLayout) findViewById(R.id.layoutTitleMaxUnknown);

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        toolbarLayout = (View) findViewById(R.id.toolbar_layout);

        btnInvite.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent invite = new Intent(MatchDetailActivity.this, InviteActivity.class);
                invite.putExtra("exMatchId", extras.getString("exMatchId").toString());
                startActivity(invite);

            }
        });

        floatEdit = (FloatingActionButton) findViewById(R.id.fab);
        floatEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MatchDetailActivity.this, EditMatchActivity.class);
                intent.putExtra("editSport", true);
                intent.putExtra("exSport" , sportCode);
                intent.putExtra("exName", getSupportActionBar().getTitle());
                intent.putExtra("exMatchId", extras.getString("exMatchId"));
                if(getIntent().getSerializableExtra("exLocal") == null){
                    intent.putExtra("exLocal", match.getLocal());
                } else {
                    intent.putExtra("exLocal", txtAddress.getText().toString());
                }
                if(getIntent().getSerializableExtra("exDate") == null){
                    intent.putExtra("exDate", match.getDateStart());
                } else {
                    intent.putExtra("exDate", exDate);
                }
                if(getIntent().getSerializableExtra("exHourBegin") != null){
                    intent.putExtra("exHourBegin", exHourBegin);
                }
                if(getIntent().getSerializableExtra("exHourEnd") != null){
                    intent.putExtra("exHourEnd", exHourEnd);
                }
                if(getIntent().getSerializableExtra("exDesc") == null){
                    intent.putExtra("exDesc", match.getDescription());
                } else {
                    intent.putExtra("exDesc", txtDesc.getText().toString());
                }
                startActivity(intent);
                finish();
            }
        });

        // Botao de lista de convidados
        btnGuestList = (Button) findViewById(R.id.btnShowPeopleList);
        btnGuestList.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MatchDetailActivity.this, GuestListActivity.class);
                intent.putExtra("exMatchId", extras.getString("exMatchId"));
                startActivity(intent);
            }
        });

        imgOptionYes = (ImageButton) findViewById(R.id.imgOptionYes);
        imgOptionYes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                imgOptionYes.setBackground(getResources().getDrawable(R.drawable.round_corner_layout, getApplicationContext().getTheme()));
                imgOptionNo.setBackground(getResources().getDrawable(R.drawable.round_corner_layout_light, null));
                imgOptionMaybe.setBackground(getResources().getDrawable(R.drawable.round_corner_layout_light, null));

                updateStatus(1);
            }
        });

        imgOptionNo = (ImageButton) findViewById(R.id.imgOptionNo);
        imgOptionNo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                imgOptionNo.setBackground(getResources().getDrawable(R.drawable.round_corner_layout, null));
                imgOptionYes.setBackground(getResources().getDrawable(R.drawable.round_corner_layout_light, null));
                imgOptionMaybe.setBackground(getResources().getDrawable(R.drawable.round_corner_layout_light, null));

                updateStatus(2);
            }
        });

        imgOptionMaybe = (ImageButton) findViewById(R.id.imgOptionMaybe);
        imgOptionMaybe.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                imgOptionMaybe.setBackground(getResources().getDrawable(R.drawable.round_corner_layout, null));
                imgOptionYes.setBackground(getResources().getDrawable(R.drawable.round_corner_layout_light, null));
                imgOptionNo.setBackground(getResources().getDrawable(R.drawable.round_corner_layout_light, null));

                updateStatus(3);
            }
        });
    }

    public void updateStatus(final int status) {
        Log.e("MATCH_DETAIL", "updateStatus");

        if (matchPlayers == null) {
            MatchPlayers newMatchPlayers = new MatchPlayers();
            newMatchPlayers.setUser(Backendless.UserService.CurrentUser());
            newMatchPlayers.setMatch(match);
            newMatchPlayers.setStatus(status);

            Backendless.Persistence.save(newMatchPlayers, new AsyncCallback<MatchPlayers>() {
                @Override
                public void handleResponse(MatchPlayers response) {
                    Log.e("MATCH_DETAIL", "updateStatus --- SAVE (SUCESSO) ");
                }

                @Override
                public void handleFault(BackendlessFault fault) {
                    Log.e("MATCH_DETAIL", "updateStatus --- SAVE (ERRO) ");
                    Toast.makeText(getApplicationContext(), "Erro ao alterar o status", Toast.LENGTH_SHORT).show();
                }
            });
        } else if (matchPlayers.getStatus() != status) {
            matchPlayers.setStatus(status);

            Backendless.Persistence.save(matchPlayers, new AsyncCallback<MatchPlayers>() {
                @Override
                public void handleResponse(MatchPlayers response) {
                    Log.e("MATCH_DETAIL", "updateStatus --- SAVE (SUCESSO) ");
                }

                @Override
                public void handleFault(BackendlessFault fault) {
                    Log.e("MATCH_DETAIL", "updateStatus --- SAVE (ERRO) ");
                    Toast.makeText(getApplicationContext(), "Erro ao alterar o status", Toast.LENGTH_SHORT).show();
                }
            });
        }
    }

    public void getMatchPlayers() {
        Log.e("MATCH_DETAIL", "getMatchPlayers");

        AsyncTask task = new AsyncTask() {
            @Override
            protected Object doInBackground(Object[] params) {
                Log.e("MATCH_DETAIL", "getMatchPlayers --- FIND ");
                BackendlessUser user = Backendless.UserService.CurrentUser();

                String userId = UserIdStorageFactory.instance().getStorage().get();
                String whereClause = "user.objectId = '"+user.getObjectId()+"' and match.objectId = '"+extras.getString("exMatchId")+"'";

                BackendlessDataQuery dataQuery = new BackendlessDataQuery();
                dataQuery.setWhereClause(whereClause);

                BackendlessCollection<MatchPlayers> result = Backendless.Persistence.of(MatchPlayers.class).find(dataQuery);
                return result;
            }

            @Override
            protected void onPostExecute(Object o) {
                Log.e("MATCH_DETAIL", "getMatchPlayers --- POST FIND ");
                BackendlessCollection<MatchPlayers> backColl = (BackendlessCollection<MatchPlayers>) o;
                if (backColl.getData().size() != 0) {
                    matchPlayers = backColl.getData().get(0);

                    if (matchPlayers.getStatus() == 1) {
                        imgOptionYes.setBackground(getResources().getDrawable(R.drawable.round_corner_layout, null));
                    } else if (matchPlayers.getStatus() == 2) {
                        imgOptionNo.setBackground(getResources().getDrawable(R.drawable.round_corner_layout, null));
                    } else {
                        imgOptionMaybe.setBackground(getResources().getDrawable(R.drawable.round_corner_layout, null));
                    }
                }
            }
        };
        task.execute();
    }

    public void fillMatchWithExtras() {
        sportCode = getIntent().getExtras().getInt("exSport");
        exName = getIntent().getExtras().getString("exName");
        exLocal = getIntent().getExtras().getString("exLocal");
        exDate = (Date) getIntent().getSerializableExtra("exDate");
        exDesc = getIntent().getExtras().getString("exDesc");
        exHourBegin = getIntent().getExtras().getString("exHourBegin");
        exHourEnd = getIntent().getExtras().getString("exHourEnd");
        exMaxUnknown = getIntent().getExtras().getInt("exMaxUnknown");
        exOpen = getIntent().getExtras().getBoolean("exOpen");

        getSupportActionBar().setTitle(exName);
        txtAddress.setText(exLocal);
        txtDesc.setText(exDesc);
        txtOwner.setText(Backendless.UserService.CurrentUser().getProperty(getString(R.string.back_user_name)).toString());
        txtMaxUnknown.setText("" + exMaxUnknown);

        floatEdit.setVisibility(View.VISIBLE);
        btnInvite.setVisibility(View.VISIBLE);


        Calendar cal = Calendar.getInstance();
        cal.setTime(exDate);

        String date = "";

        if(cal.get(Calendar.DAY_OF_MONTH) < 10){
            date += "0" + cal.get(Calendar.DAY_OF_MONTH);
        } else {
            date += cal.get(Calendar.DAY_OF_MONTH);
        }

        date += "/";

        if(cal.get(Calendar.MONTH) < 10){
            date += "0" + (cal.get(Calendar.MONTH) + 1);
        } else {
            date += (cal.get(Calendar.MONTH) + 1);
        }

        if(!exHourBegin.equals("")){
            date += " - " + exHourBegin;
        }
        if(!exHourEnd.equals("")){
            date += " ás " + exHourEnd;
        }

        txtDate.setText(date);

        if(exOpen == true){
            txtPrivate.setText("Sim");
            layoutTitleMaxUnknown.setVisibility(View.VISIBLE);
        } else {
            txtPrivate.setText("Não");
            layoutTitleMaxUnknown.setVisibility(View.GONE);
        }

        if(sportCode == Integer.parseInt(getString(R.string.sport_code_futsal))){
            txtSport.setText(getResources().getString(R.string.btn_futsal));
            toolbarLayout.setBackground(getResources().getDrawable(R.drawable.bg_futsal));
        } else if(sportCode == Integer.parseInt(getString(R.string.sport_code_society))){
            txtSport.setText(getResources().getString(R.string.btn_fut_society));
            toolbarLayout.setBackground(getResources().getDrawable(R.drawable.bg_society));
        } else if(sportCode == Integer.parseInt(getString(R.string.sport_code_soccer))){
            txtSport.setText(getResources().getString(R.string.btn_soccer));
            toolbarLayout.setBackground(getResources().getDrawable(R.drawable.bg_soccer));
        } else if(sportCode == Integer.parseInt(getString(R.string.sport_code_volley))) {
            txtSport.setText(getResources().getString(R.string.btn_volei));
            toolbarLayout.setBackground(getResources().getDrawable(R.drawable.bg_volley));
        } else if(sportCode == Integer.parseInt(getString(R.string.sport_code_basket))){
            txtSport.setText(getResources().getString(R.string.btn_basquete));
            toolbarLayout.setBackground(getResources().getDrawable(R.drawable.bg_basquete));
        } else if(sportCode == Integer.parseInt(getString(R.string.sport_code_handball))){
            txtSport.setText(getResources().getString(R.string.btn_handball));
            toolbarLayout.setBackground(getResources().getDrawable(R.drawable.bg_handball));
        } else if(sportCode == Integer.parseInt(getString(R.string.sport_code_peteca))){
            txtSport.setText(getResources().getString(R.string.btn_peteca));
            toolbarLayout.setBackground(getResources().getDrawable(R.drawable.bg_peteca));
        } else if(sportCode == Integer.parseInt(getString(R.string.sport_code_tennis))){
            txtSport.setText(getResources().getString(R.string.btn_tenis));
            toolbarLayout.setBackground(getResources().getDrawable(R.drawable.bg_tennis));
        } else if(sportCode == Integer.parseInt(getString(R.string.sport_code_paintball))){
            txtSport.setText(getResources().getString(R.string.btn_paintball));
            toolbarLayout.setBackground(getResources().getDrawable(R.drawable.bg_paintball));
        } else if(sportCode == Integer.parseInt(getString(R.string.sport_code_baseball))){
            txtSport.setText(getResources().getString(R.string.btn_basebal));
            toolbarLayout.setBackground(getResources().getDrawable(R.drawable.bg_baseball));
        } else if(sportCode == Integer.parseInt(getString(R.string.sport_code_golf))){
            txtSport.setText(getResources().getString(R.string.btn_golf));
            toolbarLayout.setBackground(getResources().getDrawable(R.drawable.bg_golf));
        } else if(sportCode == Integer.parseInt(getString(R.string.sport_code_american))){
            txtSport.setText(getResources().getString(R.string.btn_american_fut));
            toolbarLayout.setBackground(getResources().getDrawable(R.drawable.bg_american));
        } else if(sportCode == Integer.parseInt(getString(R.string.sport_code_rugby))){
            txtSport.setText(getResources().getString(R.string.btn_rugby));
            toolbarLayout.setBackground(getResources().getDrawable(R.drawable.bg_rubgy));
        } else if(sportCode == Integer.parseInt(getString(R.string.sport_code_other))){
            txtSport.setText(getResources().getString(R.string.btn_outro));
            toolbarLayout.setBackgroundColor(getResources().getColor(R.color.btnOthers));
        }
    }

    public void fillMatchWithBD() {

        // Faz a busca dos dados da partida no background
        AsyncTask task = new AsyncTask() {
            @Override
            protected Object doInBackground(Object[] params) {
                String whereClause = "objectId = '"+ extras.getString("exMatchId") +"'";
                BackendlessDataQuery dataQuery = new BackendlessDataQuery();
                dataQuery.setWhereClause(whereClause);
                BackendlessCollection<Match> result = Backendless.Persistence.of(Match.class).find(dataQuery);
                return result;
            }

            @Override
            protected void onPostExecute(Object o) {
                BackendlessCollection<Match> backColl = (BackendlessCollection<Match>) o;
                match = backColl.getData().get(0);

                // Se nao for o organizador, esconde botão de editar
                BackendlessUser user = Backendless.UserService.CurrentUser();
                if (match.getOwner().getObjectId().toString().equals(user.getObjectId().toString())) {
                    floatEdit.setVisibility(View.VISIBLE);
                    btnInvite.setVisibility(View.VISIBLE);

                }

                setMatchFields(match);

                screenLoading.setVisibility(View.GONE);
                screenElements.setVisibility(View.VISIBLE);
            }
        };
        task.execute();

        // Enquanto carrega no backrground, executa a baixo
        // Define o nome da partida
        String title = extras.getString("exMatchName");
        getSupportActionBar().setTitle(title);

        // Define imagem
        sportCode = extras.getInt("exSport");
        if(sportCode == Integer.parseInt(getString(R.string.sport_code_futsal))){
            txtSport.setText(getResources().getString(R.string.btn_futsal));
            toolbarLayout.setBackground(getResources().getDrawable(R.drawable.bg_futsal));
        } else if(sportCode == Integer.parseInt(getString(R.string.sport_code_society))){
            txtSport.setText(getResources().getString(R.string.btn_fut_society));
            toolbarLayout.setBackground(getResources().getDrawable(R.drawable.bg_society));
        } else if(sportCode == Integer.parseInt(getString(R.string.sport_code_soccer))){
            txtSport.setText(getResources().getString(R.string.btn_soccer));
            toolbarLayout.setBackground(getResources().getDrawable(R.drawable.bg_soccer));
        } else if(sportCode == Integer.parseInt(getString(R.string.sport_code_volley))) {
            txtSport.setText(getResources().getString(R.string.btn_volei));
            toolbarLayout.setBackground(getResources().getDrawable(R.drawable.bg_volley));
        } else if(sportCode == Integer.parseInt(getString(R.string.sport_code_basket))){
            txtSport.setText(getResources().getString(R.string.btn_basquete));
            toolbarLayout.setBackground(getResources().getDrawable(R.drawable.bg_basquete));
        } else if(sportCode == Integer.parseInt(getString(R.string.sport_code_handball))){
            txtSport.setText(getResources().getString(R.string.btn_handball));
            toolbarLayout.setBackground(getResources().getDrawable(R.drawable.bg_handball));
        } else if(sportCode == Integer.parseInt(getString(R.string.sport_code_peteca))){
            txtSport.setText(getResources().getString(R.string.btn_peteca));
            toolbarLayout.setBackground(getResources().getDrawable(R.drawable.bg_peteca));
        } else if(sportCode == Integer.parseInt(getString(R.string.sport_code_tennis))){
            txtSport.setText(getResources().getString(R.string.btn_tenis));
            toolbarLayout.setBackground(getResources().getDrawable(R.drawable.bg_tennis));
        } else if(sportCode == Integer.parseInt(getString(R.string.sport_code_paintball))){
            txtSport.setText(getResources().getString(R.string.btn_paintball));
            toolbarLayout.setBackground(getResources().getDrawable(R.drawable.bg_paintball));
        } else if(sportCode == Integer.parseInt(getString(R.string.sport_code_baseball))){
            txtSport.setText(getResources().getString(R.string.btn_basebal));
            toolbarLayout.setBackground(getResources().getDrawable(R.drawable.bg_baseball));
        } else if(sportCode == Integer.parseInt(getString(R.string.sport_code_golf))){
            txtSport.setText(getResources().getString(R.string.btn_golf));
            toolbarLayout.setBackground(getResources().getDrawable(R.drawable.bg_golf));
        } else if(sportCode == Integer.parseInt(getString(R.string.sport_code_american))){
            txtSport.setText(getResources().getString(R.string.btn_american_fut));
            toolbarLayout.setBackground(getResources().getDrawable(R.drawable.bg_american));
        } else if(sportCode == Integer.parseInt(getString(R.string.sport_code_rugby))){
            txtSport.setText(getResources().getString(R.string.btn_rugby));
            toolbarLayout.setBackground(getResources().getDrawable(R.drawable.bg_rubgy));
        } else if(sportCode == Integer.parseInt(getString(R.string.sport_code_other))){
            txtSport.setText(getResources().getString(R.string.btn_outro));
            toolbarLayout.setBackgroundColor(getResources().getColor(R.color.btnOthers));
        }
    }

    public void setMatchFields(Match match) {
        Log.e("MATCH_DETAIL", "setMatchFields --- MATCH OWNER = " + match.getOwner().getProperty(getString(R.string.back_user_name).toString()));
        if (match.getGeopoint() != null) {
            txtAddress.setText(getCompleteAddressString(match.getGeopoint().getLatitude(), match.getGeopoint().getLongitude()));
        }
        txtDesc.setText(match.getDescription());
        txtOwner.setText(match.getOwner().getProperty(getString(R.string.back_user_name)).toString());

        Calendar cal = Calendar.getInstance();
        cal.setTime(match.getDateStart());
        int selectedHour = 0 , selectedMinute = 0;

        cal.set(Calendar.HOUR_OF_DAY, selectedHour);
        cal.set(Calendar.MINUTE, selectedMinute);


        String date = "";

        if (cal.get(Calendar.DAY_OF_MONTH) < 10) {
            date += "0" + cal.get(Calendar.DAY_OF_MONTH) ;
        } else {
            date += cal.get(Calendar.DAY_OF_MONTH) ;
        }

        date += "/";

        if (cal.get(Calendar.MONTH) < 10) {
            date += "0" + (cal.get(Calendar.MONTH) + 1);
        } else {
            date += (cal.get(Calendar.MONTH)+ 1);
        }

        date += " - ";

        if(cal.get(Calendar.HOUR_OF_DAY) < 10){
            date += "0" + cal.get(Calendar.HOUR_OF_DAY);
        } else {
            date += cal.get(Calendar.HOUR_OF_DAY);
        }
        date += ":";
        if(cal.get(Calendar.MINUTE)< 10) {
            date += "0" + cal.get(Calendar.MINUTE);
        } else {
            date += cal.get(Calendar.MINUTE);
        }

        if(match.getDateEnd() != null){
            cal.setTime(match.getDateEnd());
            date += " ás ";
            if(cal.get(Calendar.HOUR_OF_DAY) < 10){
                date += "0" + cal.get(Calendar.HOUR_OF_DAY);
            } else {
                date += cal.get(Calendar.HOUR_OF_DAY);
            }
            date += ":";
             if(cal.get(Calendar.MINUTE)< 10) {
                 date += "0" + cal.get(Calendar.MINUTE);
             } else {
                 date += cal.get(Calendar.MINUTE);
             }
        }

        txtDate.setText(date);

        if(match.isOpen()){
            txtPrivate.setText("Sim");
        } else {
            txtPrivate.setText("Não");
        }

        sportCode = match.getSport();
        if(sportCode == Integer.parseInt(getString(R.string.sport_code_futsal))){
            txtSport.setText(getResources().getString(R.string.btn_futsal));
            toolbarLayout.setBackground(getResources().getDrawable(R.drawable.bg_futsal));
        } else if(sportCode == Integer.parseInt(getString(R.string.sport_code_society))){
            txtSport.setText(getResources().getString(R.string.btn_fut_society));
            toolbarLayout.setBackground(getResources().getDrawable(R.drawable.bg_society));
        } else if(sportCode == Integer.parseInt(getString(R.string.sport_code_soccer))){
            txtSport.setText(getResources().getString(R.string.btn_soccer));
            toolbarLayout.setBackground(getResources().getDrawable(R.drawable.bg_soccer));
        } else if(sportCode == Integer.parseInt(getString(R.string.sport_code_volley))) {
            txtSport.setText(getResources().getString(R.string.btn_volei));
            toolbarLayout.setBackground(getResources().getDrawable(R.drawable.bg_volley));
        } else if(sportCode == Integer.parseInt(getString(R.string.sport_code_basket))){
            txtSport.setText(getResources().getString(R.string.btn_basquete));
            toolbarLayout.setBackground(getResources().getDrawable(R.drawable.bg_basquete));
        } else if(sportCode == Integer.parseInt(getString(R.string.sport_code_handball))){
            txtSport.setText(getResources().getString(R.string.btn_handball));
            toolbarLayout.setBackground(getResources().getDrawable(R.drawable.bg_handball));
        } else if(sportCode == Integer.parseInt(getString(R.string.sport_code_peteca))){
            txtSport.setText(getResources().getString(R.string.btn_peteca));
            toolbarLayout.setBackground(getResources().getDrawable(R.drawable.bg_peteca));
        } else if(sportCode == Integer.parseInt(getString(R.string.sport_code_tennis))){
            txtSport.setText(getResources().getString(R.string.btn_tenis));
            toolbarLayout.setBackground(getResources().getDrawable(R.drawable.bg_tennis));
        } else if(sportCode == Integer.parseInt(getString(R.string.sport_code_paintball))){
            txtSport.setText(getResources().getString(R.string.btn_paintball));
            toolbarLayout.setBackground(getResources().getDrawable(R.drawable.bg_paintball));
        } else if(sportCode == Integer.parseInt(getString(R.string.sport_code_baseball))){
            txtSport.setText(getResources().getString(R.string.btn_basebal));
            toolbarLayout.setBackground(getResources().getDrawable(R.drawable.bg_baseball));
        } else if(sportCode == Integer.parseInt(getString(R.string.sport_code_golf))){
            txtSport.setText(getResources().getString(R.string.btn_golf));
            toolbarLayout.setBackground(getResources().getDrawable(R.drawable.bg_golf));
        } else if(sportCode == Integer.parseInt(getString(R.string.sport_code_american))){
            txtSport.setText(getResources().getString(R.string.btn_american_fut));
            toolbarLayout.setBackground(getResources().getDrawable(R.drawable.bg_american));
        } else if(sportCode == Integer.parseInt(getString(R.string.sport_code_rugby))){
            txtSport.setText(getResources().getString(R.string.btn_rugby));
            toolbarLayout.setBackground(getResources().getDrawable(R.drawable.bg_rubgy));
        } else if(sportCode == Integer.parseInt(getString(R.string.sport_code_other))){
            txtSport.setText(getResources().getString(R.string.btn_outro));
            toolbarLayout.setBackgroundColor(getResources().getColor(R.color.btnOthers));
        }
    }

    @Override
    public void onBackPressed() {
        finish();
    }

    private String getCompleteAddressString(double LATITUDE, double LONGITUDE) {
        Address returnedAddress = null;
        Geocoder geocoder = new Geocoder(this, Locale.getDefault());
        try {
            List<Address> addresses = geocoder.getFromLocation(LATITUDE, LONGITUDE, 1);
            if (addresses != null) {
                returnedAddress = addresses.get(0);
            } else {
                Log.w("My Current loction address", "No Address returned!");
            }
        } catch (Exception e) {
            e.printStackTrace();
            Log.w("My Current loction address", "Cannot get Address!");
        }
        return returnedAddress.getAddressLine(0);
    }
}
